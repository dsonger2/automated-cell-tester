**ECE445 ONLINE LAB NOTEBOOK**

WONJUN LEE (wonjunl2)

**Meeting_And_Working_TimeLine**
<details><summary>9/14/21 - TA MEETING</summary>

In order to understand the performance of the battery, it is important to have certain characteristics when the temperature changes. 
Raising the temperature of the battery is expected to be easy, whereas lowering the temperature is expected to be difficult.
we explained the hardest part of our project -> COOLING SYSTEM

PELTIER DEVICE -> TA suggested this device to cool down our battery.

We need to do some research because we do not want to make fridge for this project.

</details>

<details><summary>9/15/21 - PCB Design</summary>
We have talked about our PCB Design. (BJT vs MOSFET)

![](Bipolar-Junction-Transistor.png)

BJT -> why?
To amplify current it will allow BJTs are used as amplifiers or switches to produce wide applicability in electronic equipment.

The one thing that we need to implement is heat/cooling system.

![](Peltier-effect.jpg)

**Peltier device** -> electronic devices designed for cooling objects to below the ambient temperature or maintaining objects at a specific temperature by controlled heating or cooling. But We are not sure whether we can maintaining the temperature in the course of the test.

we can use peltier device to make the battery cool and hot. -> maybe make the whole device small? Not sure.

CHIP Candidate: SDA, on-chip temperature sensor, and so on.
-> **ATmega32u4** 
It will detect changes in temperature and send the corresponding signal to the main controller.

we might need this too -> LTC2942 -> charging and columb counting
Compared to others, it seems to be reasonable in performance or price.

Resource: 
1. - https://global.kyocera.com/prdct/ecd/peltier/ 
2. - https://components101.com/articles/understanding-bjt-transistor-and-how-to-use-it-in-your-circuit-designs 

</details>

<details><summary>9/16/21 - Machine Shop Meeting</summary>
MACHINE_SHOP_MEETING

We have talked about how to implement the whole system briefly.

We got some suggestions that the outlayer of our product(box) could be WOOD.

The machine shop said that a box made of aluminum with excellent heat conduction is suitable for our project. 

Perhaps we should talk more about the exterior wall of this device, but if we use wood, it will be more difficult to connect the wires later. 

However, considering the aesthetic part, it is worth trying.

![](thermal_chamber.jpeg)


</details>

<details><summary>9/19/21 - Communication between Main Controller and Device + Determine other components</summary>

We have talked about bluetooth communication.

We are considering use Labview, Java, or Python. -> probably Python...? 

I think we will use python becasue it has a lot of useful modules and library (Scipy, PySerial, ...).

Analog signal read -> get the signal through bluetooth -> present on the webApp.

Webapp should be ready before the machine is ready. THERE IS NO TIME!

I need to figure it out how bluetooth works and make the fancy webApp.

Graphical interface for users -> Menu + download PDF + Tables + Charts + ...

Also we are gonna use the following components:
1. DAC: TLV5618-EP - It has the necessary temperature range and buffered outputs.
2. Li-ion charger IC: BQ24179 - This satisfies our temperature requirements.

Resource:

1. https://www.ti.com/lit/ds/symlink/tlv5638-ep.pdf?ts=1632088230674&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTLV5638-EP
2. https://www.ti.com/lit/ds/symlink/bq24179.pdf?ts=1632031521794


</details>

<details><summary>9/22/21 - Choose main power bjt and finalize analog biasing circuitry</summary>

Before completing the design document and the first pcb board, we decided to focus more on the hardware part.

We start KiCAD to design pcb schematic.

We talked about the locations of op-amps and battery isolation.

We talekd about testing on a smaller battery. 
     -> As the size of the battery to be tested decreases, the design of the final device of the project may also decrease. 
     I think we should ask this to ta.

A module for Bluetooth connection should be determined. 
Many projects seem to use **hc-05**. 
Our user interface must communicate directly with the main controller and show the results directly on the computer.


Resource:

1. https://components101.com/sites/default/files/component_datasheet/HC-05%20Datasheet.pdf 


</details>

<details><summary>9/24/21 - Weekly TA Meeting</summary>

We discussed our team roles for the next week.

Use solid state relays -> fast/smooth transition between charging and discharging

Joon -> WebApp + Bluetooth Communication

</details>

<details><summary>9/26/21 - High Level Design + Design Document Draft</summary>

The computer system should include the bluetooth connection from device to the computer itself as well as the user interface.
The device should be cross-platform and also allow user to store data from the tests on the local machine.
The user interface should load the previous test result and compare to other previous test results.

The Requirement and verifications are as followed:
Requirement:
1. Relay information about the given test or the start/end of a test to the users computer over Bluetooth.
2. Web application should store the data and load the data on the local computer.
3. Web applicatoin should show the live data on the screen.

Verifications:
1. Bluetooth live message time signiture be less than 8ms.
2. The data and graph are aesthetic, easy to understand.
3. The web application should updates the test page at least every 500ms.


</details>

<details><summary>9/27/21 - Design Document Check</summary>

TA and Professor says we can test mush smaller batteries.
-> smaller thermal unit 
-> smaller thermal test range 

</details>

<details><summary>9/28/21 - PCB Review</summary>

TA says that serparate power supply -> high power + low power

We decided to use two power supply as followed:
1. Low power suply: AZ1117C
2. High power suply: MIC29751-5.0BWT

Resource:

1. https://www.diodes.com/assets/Datasheets/AZ1084C.pdf
2. https://rocelec.widen.net/view/pdf/cqkinzilfg/MCRLS04602-1.pdf?t.download=true&u=5oefqw


</details>

<details><summary>10/01/21 - Weekly TA Meeting</summary>

I did not test Webapp at all.

We discussed the cost of solid state relays. -> expensive -> change to physical relays?


</details>

<details><summary>10/04/21 - Bluetooth Test</summary>

I start the bluetooth Test with HC-05 and Arduino Mega.

Since the HC-05 is only working with Android and Windows OS.

I used MIT App Inventor emulator to test the bluetooth communication.

I tested receiving data and creating chart on the emulator.

I used one potentiometer to adjust the input.

This is my code as followed:

```
# code block
int in = A0;
void setup() {
  Serial.begin(9600);
  pinMode(in,INPUT);

}
void loop() {
  //Map the value from 10 bits to 8 bits:
  
  //Byte = 8 bits = 2 to the 8 = 255  
  byte val = map(analogRead(in),0,1024.0,0,255); 

  //Serial.write will send only one byte at a time
  Serial.write(val);

  //Small delay between each data send  
  delay(400);                                        
}
```

This is the schematic from the resource.
I used HC-05, not HC-06.
![](test1-schematic.png)

Buglog: The bluetooth connection is failed during the test. I cannnot login.
Fix: Since the connection is failed, I really need to change the connection plan.
     Maybe this is because I used HC-05 instead of HC-06?

Resource:

1. https://www.arduino.cc/en/Hacking/PinMapping2560
2. http://electronoobs.com/eng_arduino_tut20_1.php

</details>

<details><summary>10/06/21 - Design Review</summary>

We attended design Review.

Feedback 
1. make the thermal unit smaller
2. safety considerations for discharging the battery.

The bluetooth communication is still not working.

</details>

<details><summary>10/14/21 - Working on Webapp design</summary>

We need to have a valid webapp as we expected.

This is the design of the webapp.
![](webapp-design.png)

We want to have 4 pages.
1. Main page - consist of live number data.
2. Start test page - start and stop the test
3. History of test - can load the specific test.
4. Live test data - show the current test data on screen.


</details>


<details><summary>10/15/21 - Weekly TA Meeting </summary>

Stasiu says that he wanted to see the web application.


</details>

<details><summary>10/21/21 - Machine Shop Meeting </summary>

We sent a qauntified blueprint to machine shop.


</details>

<details><summary>10/22/21 - Weekly TA Meeting</summary>

We talked about the webapp. -> I did not finish the webapp to show.
I designed the webapp as followed:

![](WebApp-Design.png)



</details>


<details><summary>10/28/21 - Machine Shop Meeting </summary>

Revised thermal unit -> building the inside of the box using aluminum.


</details>


<details><summary>11/05/21 - Weekly TA Meeting </summary>

We talked about the state of our project.
We were worried about our software part since I did not get how to implement it.

Daniel and I worked on python gui application.

We decided to use Pyserial and Tkinter.

Small test to make python gui application.

The following python code is the simple blank python gui application.

```
# code block

from tkinter import * # module input
 
root = Tk() # create root(home) window
 
root.title("ECE445 Python GUI") # root window title and dimension

root.geometry('350x200') # Set Width x Height

lbl = Label(root, text = "Wanna get A?") #adding a label to the root window
lbl.grid()
 
# Execute Tkinter
root.mainloop()

```
This is the result of the code:

![](pythongui.png)



Resource:

1. https://docs.python.org/3/library/tkinter.html
2. https://pypi.org/project/pyserial/ 

</details>

<details><summary>11/11/21 - Decide the communication format</summary>

Daniel and I talked about the serial communication insteand of bluetooth.

Since our device become smaller, there is no need to use bluetooth communication.

And the bluetooth communication has security issue.

Therefore, we decided to try a simpler, faster, and more secure serial port.

</details>

<details><summary>11/12/21 - Weeky TA Meeting </summary>

We talked about web application and bluetooth communication.

I am still not sure how to read the data via bluetooth. -> change to HC-06? or Wireless module?

If we want to change to serial port communication then we have to decided soon.


</details>

<details><summary>11/14/21 - Preparing Mock Demo </summary>

We decided to use PySerial and Python Gui.

We decided to use 4 different data types.

1. DATA_MESG = 0
2. ERROR_MESG = 1
3. START_MESG = 2
4. STOP_MESG = 3

We implemented the structure of Python Gui.

Here is the part of main.py file.

```
# code block

def read_data():

    time = 0
    type = 0
    temp = 0
    voltage = 0
    current = 0

    data = arduino.read(11)
    data_arr = []
    for byte in data:
        data_arr.append(byte)

    if len(data_arr) == 11:
        type = data_arr[0]
        time = (data_arr[1] << 24) + (data_arr[2] << 16) + (data_arr[3] << 8) + data_arr[4]
        voltage = (data_arr[5] << 8) + data_arr[6]
        current = (data_arr[7] << 8) + data_arr[8]
        temp = (data_arr[9] << 8) + data_arr[10]

    return len(data_arr), type, float(time / 1000.0), float(voltage / 1000.0), float(current / 1000.0), float(temp / 1000.0)
```

```
# code block

class HomePage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#404040")
        label = tk.Label(self, text="Home", font=LARGE_FONT)
        label.configure(background="#181850", foreground="#FFFFFF")
        label.pack(pady=10, padx=10)

        button = ttk.Button(self, text="Test Start Page",
                            command=lambda: controller.show_frame(TestStartPage))
        button.pack()

        button2 = ttk.Button(self, text="Analyze Cell Data",
                             command=lambda: controller.show_frame(AnalyzeDataPage))
        button2.pack()

        button3 = ttk.Button(self, text="Live Graph Data",
                             command=lambda: controller.show_frame(GraphDataPage))
        button3.pack()

        current = tk.Label(self, text="Current:", font=LARGE_FONT)
        current.configure(background="#002A6E",foreground="#FFFFFF")
        current.place(x=100, y=240)
        self.currentText = tk.Label(self, text="0")
        self.currentText.configure(background="#002A6E",foreground="#FFFFFF")
        self.currentText.place(x=115, y=260)

        voltage = tk.Label(self, text="Voltage:", font=LARGE_FONT)
        voltage.configure(background="#002A6E",foreground="#FFFFFF")
        voltage.place(x=350, y=240)
        self.voltageText = tk.Label(self, text="0")
        self.voltageText.configure(background="#002A6E",foreground="#FFFFFF")
        self.voltageText.place(x=365, y=260)

        temperature = tk.Label(self, text="Temperature:", font=LARGE_FONT)
        temperature.configure(background="#002A6E",foreground="#FFFFFF")
        temperature.place(x=600, y=240)
        self.temperatureText = tk.Label(self, text="0")
        self.temperatureText.configure(background="#002A6E",foreground="#FFFFFF")
        self.temperatureText.place(x=615, y=260)

```


Still need to implement start and end the test function.


</details>

<details><summary>11/17/21 - Preparing Mock Demo </summary>

We implement the test start page. - not fully functional.

```
# code block

class TestStartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#404040")
        label = tk.Label(self, text="Start a test", font=LARGE_FONT)
        label.pack(pady=10, padx=10)
        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(HomePage))
        button1.pack()
        #creating list for test options
        TEST_OPTIONS = ["Test Options","Capacity", "SoC-OCV", "HPPC"]
        test_options = ttk.Combobox(self, value=TEST_OPTIONS)
        test_options.current(0)
        test_options.pack(pady = 40, padx = 40)
        test_options.place(x=115, y=240)
        #creating list for temp options
        TEMP_OPTIONS = ["Temperature Options","Hot", "Room tempterature", "Cold"]
        temp_options = ttk.Combobox(self, value=TEMP_OPTIONS)
        temp_options.current(0)
        temp_options.pack(pady=40, padx=40)
        temp_options.place(x=485, y=240)
        #test start button
        button2 = ttk.Button(self, text="Start Test",
                             command=lambda: send_start_mesg(test_type=test_options.get(), desired_temp=temp_options.get()))
        button2.pack(pady = 30, padx = 30)
        button2.place(x=350, y=400)
        button3 = ttk.Button(self,text = "Stop Test",command = lambda: send_stop_mesg())
        button3.pack(pady = 40, padx = 40)
        button3.place(x=350, y=425)

```

This code will generate the test start and stop message to main controller.


</details>

<details><summary>11/18/21 - Preparing Mock Demo </summary>

buglogs: sending signal is not working. -> why? software problem or hardware problem?

Still not figure it out how to solve it.

</details>

<details><summary>11/19/21 - Mock Demo</summary>

We attended Mock Demo.

We still need to implement the temperature function and fix the bugs.


</details>

<details><summary>11/29/21 - Preparing Demo </summary>

We implemented the whole Python Gui Application.

See the 'code-PythonGUI' folder.

The device sent appropriate message to device but the device did not getting hot when it received 'hot' message.



</details>

<details><summary>11/30/21 - Preparing Demo </summary>

One of our Arduino is broken.

I tried to fix the signal issue but my laptop and the Arduino shut down suddenly.

The Arduino looked ok but the laptop could not read the signal input.

We found another Arduino and connected to main controller.

This is the test result.

![](Battery-result)
![](pythonGui-livedata)


</details>

<details><summary>12/01/21 - Final Demo</summary>

I was really embarrassed to be late for the final presentation.

We did well on our final demo.

We worked on mock presentation slides.

</details>

<details><summary>12/02/21 - Mock Presentation</summary>

I did not attended mock presentation.....
Feedback: tried to use less filter words.


</details>

<details><summary>12/05/21 - Prepare for Final Presentation</summary>

we worked on the final presentation slides.

Split the part of the presentation.

Joon -> introduction + User interface

</details>

<details><summary>12/06/21 - Prepare for Final Presentation</summary>

We met at the ECEB and rehearse final presentation.

Finalize the presentation slides.

</details>

<details><summary>12/07/21 - Final Presentation</summary>

We had our final presentation at 8am.


</details>

<details><summary>12/08/21 - Finalize Final Report</summary>

We wrote the final Report.


</details>
