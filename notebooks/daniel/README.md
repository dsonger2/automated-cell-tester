## Daniel's notebook (dsonger2)
### ENTRIES:
**8-31-21** <br />
We met to discuss the project and its goals.
mostly we simply discussed the project's battery theory theory to gain enough background information to discuss the details of the specific current tests we will run.
We believe we want o to use an array of power resistors for bleeding specific current from the cell, as well as an accurate current sensor.
WE also briefly discussed the use of the ESP32 chip as it is nicely configured for our purposes as well as specially suited for our thermal application.

- LTC2942 IC for charging
- ARJ A1 power resistor

**9-7-21** <br />
Today we talked about:
1) Fire safety training
2) soldering assignment
3) weekly meeting times - landed on Wednesday afternoons + alternative weekend meeting.
4) Assigned some roles for turning in the proposal.
5) Discussed wanting a digital notebook

**9-12-21** <br />
Discussed at length the various subsystems and how many things fit together. 
Subsystems:
1) Battery component
2) Power controls
3) Thermal unit
4) Thermal controls
5) IO including on the board (prefer i2c)
6) Testing routines
Discussed more on the project proposal and necessary steps to completion.

**9-25-21** <br />
We worked mostly on our first draft of the PCB (seen below).
![PCB First Draft](pcb_firstdraft.png)
To do this we first though about what we would want to see the circuit doing by the deadline in a couple weeks.
1) Thermal control with bipolar peltier device
2) Wide range of current discharge control
3) Bluetooth comms up and runnning
4) ADCs being read to and from
5) current sensing ADC
6) I2C comms ready

We decided on a few components listed below:
1) ATmega32u4 - microcontroller selected for its USB protocol and 16 10-bit ADCs
2) LTC2942 - charging IC for coulomb counting and voltage regulation

We decided that most high-precision ADCs would need to be at least 12-bit ADCs and DACs so we are implementing external analog electronics controlled over I2C.
We also decided to use some form of fuses to isolate the battery as well as physical separation of the board that deals directly with the battery.

**9-17-21** <br />
FIRST MACHINE SHOP VISIT
We discussed with Gregg the idea and he thought it was interesting. 
We are going to choose a material for the thermal unit that is sturdy but most importantly, thermally efficient. Peltier devices can use all the help they can get.
For this, we think it would be necessary to have Peltier devices mounted on an interior wall of the thermal chamber with the opposite side on the exterior side of the chamber.
Then we will route a way to pull cool air past the devices ideally keeping them more thermally neutral on the side that is not being used for the thermal chamber (i.e. If we want the chamber to be cold, the outside of the Peltier deivces will become hot, and we will want to cool them by pulling cool air past them constantly). 
We will likely use computer fans to do any ventilation.
![Thermal Chamber Drawing](thermal_chamber.jpeg)

**9-19-2021** <br />
Discussion that led us to switch to a web app instead of LabView. Jun took over this end of the devices functionality.
We also decided to separate out the thermal unit into its own controller architecture. This way we can tell this part of the circuit what control scheme for temperature we need and leave keep valuable cycles on the main processor.
We think the ATTiny85 could perform this function.

Thermistors will be necessary to accomplish any sensing of the ambient temperature inside the thermal chamber. This will also reaquire another ADC.
The battery charging IC we have picked (see below) also comes with built in thermal sensing for the battery cell itself in the case of an emergency shutdown. This means that the charger IC implements the necessary safety features we are looking for given that this project has heavy thermal applications.

We will be using the following components:
1) DAC: TLV5618-EP ([datasheet](https://www.ti.com/lit/ds/symlink/tlv5638-ep.pdf?ts=1632088230674&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTLV5638-EP)) because it has the necessary temperature range and buffered outputs.
2) Li-ion charger IC: BQ24179 ([datasheet](https://www.ti.com/lit/ds/symlink/bq24179.pdf?ts=1632031521794)) this charges at 5A, max/min voltage is 3V to 18.8V and satisfies our temperature requirements.

**9-22-21** <br />
Today we discussed some of the final touches on our discharging circuit such as locations of op-amps and battery isolation. We have not yet finalized the charger circuit as this will be an IC and it is rather hard to find one that we like.
We also discussed bluetooth progress and web app development. Finally, we discussed meeting for actual breadboarding and testing of circuit design on a much smaller battery. 
I have been playing around in arduino to control ADCs and DACs as well as develop the timer/interrupt usage for our code.
One final thing I have down outside the lab today is develop some structure for how the overall program and user loop will operate.
![Arduino High Level Code](arduino_high_level.jpeg)

**9-24-2021** <br />
We met with our TA today. We answered a few questions in the following form:
1) Use solid state relays for fast/smooth transitions between charging and discharging circuit.
2) Use a charging IC with any form of mounting, we will have alternative soldering resources made available to us.

We also discussed team roles for the next week.

**9-26-2021** <br />
We finalized a few schematics for first PCB round. To do this, we needed to decide which parts we were going to use.
We settled on the following:
1) ADC: ADC121C021 ([datasheet](https://www.ti.com/lit/ds/symlink/adc121c021.pdf)) with more channels.
2) op-amp: MCP6V69 ([datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/MCP6V66-Family-Data-Sheet-DS20006266A.pdf)) with more at least two full op-amps per chip
3) SS relay: CMX60D10 ([datasheet](https://media.ncd.io/20190214164009/cmx-series-dc-pcb-mount.pdf)) will need a couple of these, 10A, 3-10VDC

**9-27-2021** <br />
Decided on power supply while going to layout: 
1) AZ1117C at 5.0V ([datasheet](https://www.diodes.com/assets/Datasheets/AZ1084C.pdf)) for the low power devices.
2) MIC29751-5.0BWT ([datasheet](https://rocelec.widen.net/view/pdf/cqkinzilfg/MCRLS04602-1.pdf?t.download=true&u=5oefqw)) for the high power consumption portions.

**10-1-2021** <br />
Our weekly TA meeting revealed the necessity for physical relays rather than solid state relays due to the necessity that they must drive so much current and are too expensive.


**10-3-2021** <br />
Working on the schematic. Decided on this high-capacity relay: JW1FSN-DC5V ([datasheet](file:///Users/danieljsonger/Downloads/mech_eng_jw.pdf))

**10-8-2021** <br />
shunt power resistor at 50 mOhms ±1%, rated for 50W, and a low enough tolerance for our sensor:
50WMD05 ([datasheet](https://www.digikey.com/en/products/detail/nte-electronics-inc/50WMD05/11652197))
also added this preliminary BOM not including anything from thermal control, sans the buffer and DAC. 
![BOM first draft](ece445_BOM_v1.png)

**10-14-21** <br />
We talked about the need for a new IC and established that this would be the focus of the project for now. We discussed dropping the amperage requirement to make finding the right IC package for our application a broader search.


**10-22-21** <br />
We checked that our plan to use the MAX77976EFD+ ([datasheet](https://datasheets.maximintegrated.com/en/ds/MAX77975-MAX77976.pdf)) was ok with Stasiu and proceeded to plan how to do the pcb in one round of ordering.


**10-27-2021** <br />
As the code gets intense, I wanted to keep track of some resources:
1) SAMD_TimerInterrupt ([link](https://github.com/khoih-prog/SAMD_TimerInterrupt/blob/main/examples/TimerInterruptLEDDemo/TimerInterruptLEDDemo.ino))
2) Timers and Interrupts in Arduino ([link](https://www.robotshop.com/community/forum/t/arduino-101-timers-and-interrupts/13072))
3) Arduino to breadboard ([link](https://www.arduino.cc/en/Tutorial/BuiltInExamples/ArduinoToBreadboard))
4) Bootloading Arduino on ATMega32u4 ([link](https://electronut.in/bootloader-atmega32u4/))
5) Sparkfun Bootloader ([link](https://github.com/sparkfun/SF32u4_boards))


**11/1/2021** <br />
An equivalent circuit model for battery characteristic estimation. The ecm has a function for computing RC tau time constants in python. We will use this in our web application.
github for ecm: [link](https://github.com/batterysim/equiv-circ-model)

**11/5/2021** <br />
Stasiu walked us through what needs to get done as our first PCB arrives. We are planning to build it quickly and begin testing functionality such as safety and dishcarge right away.
Next we will test charge and sensors and then fine tune everything for accuracy. With that, we could be ready for our mock demo.
I have included some notes on what is still in TODO phase of the code.
![Code TODO](code_todo.png)

**12/6/2021** <br />
Lots of debugging later (in which the many datasheet links on this notebook were very helpful) ... We essentially made only the thermal unit work as expected, but could never get the control to store any code.
As it was the most working part of our design, John and I ran some tests that resulted in the following [spreadsheet](https://docs.google.com/spreadsheets/d/1QOqS0okPGffpPJiTWQ46FPa26y28CuiVm4PoMtCYX34/edit#gid=0).
PS: Thanks for a great semester Stasiu! We had a lot of fun and learned a lot from you.
