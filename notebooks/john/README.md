9-1-21
Objective: Meet Team and Discuss Project Concept

 - Met Daniel and Joon

 - Discussed battery tests and how they're performed

 - Discussed how to discharge constant current from battery regardless of Soc-Voc (state of charge - open circuit voltage)


9-10-21
Objective: First TA (Stasiu) Meeting

 - Met with Stasiu via zoom


9-12-21
Objective: Begin High Level Design

 - Discussed BJT discharge circuit topology (voltage input to base proportional to discharge current) as option (I = (Vin-Vbe)/R)Beta)

 - Discussed Coulomb counting algorithm for battery capacity test.

 - Examined possible prepackaged combination voltage current sensors

9-14-21
Objective: Create Block Diagram Based on High Level Design

 - Created first draft of block diagram (see block diagram draft png in notebook folder)

9-17-21
Objective: Meet with TA (Stasiu)

 - met with Stasiu via zoom


9-19-21
Objective: Continue High Level Design

 -Settled on DAC (TLV5618) selected for resolution
DATASHEET (https://www.ti.com/lit/ds/symlink/tlv5638-ep.pdf?ts=1632088230674&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTLV5638-EP)

 -Settled on MCU (ATmega32u4)

 -Explored Resourse availible in senior design Lab

 -Settled on BJT topology for discharge circuit

 -Decided to design custom voltage and current sensor


9-21-21
Objective: Choose main power bjt and finalize analog biasing circuitry

 -Chose MJ21194 BJT, for highpower rating, large forward active region, and low leakage current
DATASHEET(chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/viewer.html?pdfurl=https%3A%2F%2Fwww.onsemi.com%2Fpdf%2Fdatasheet%2Fmj21193-d.pdf&clen=152948)

 -Abandoned single resistor buffered DAC on BJT base in favor of dual resistor switched approach

 -Resistors valued 1k ohms and 20 ohms switched between via an nmos pmos pair

 -made considerations for current sensing resistor. Settled on 0.25 ohms with x4 opam to ADC. 

 -simulated circuit in LTspice and uploaded files to GitLab. Placeholder cmos used, actual component specs relatively unimportant (see Dishcharge Circuit Layout.png)


9-22-21
Objective: Discuss Finalized Analog Biasing Circuitry

 - Showed Daniel the circuitry and simulation data designed the previous day.

 - Got Daniel's feedback on resistor sizing

 - Began KiCAD schematic
 
 - Settled on general purpose op-amp (AD8592ARMZ-REEL) selected for max current rating of 250 mA
DATASHEET(https://www.analog.com/media/en/technical-documentation/data-sheets/AD8591_8592_8594.pdf)


9-24-21
Objective: Meet with TA (Stasiu)

 - Discussed current switching methods to isolate charging and discharging circuits. Stasiu suggested solid state relays.


9-26-21
Objective: Complete Design Doc Rough Draft / KiCAD Schematics

 - Worked on design block sections of design doc (discharge, sensor suite, thermal control unit, and power supply)

 - Worked on KiCAD Schematics

	- Completed Schematics: Discharge, Charge, and Sensor Suite (see corresponding pngs in notebook folder NOTE: SEE ORIGINAL CHARING PNG)

	- Sensor Suite implements a differential amplifier referenced to 2.5 v for current sensing. Amplifies difference between voltages on either side of a shunt resistor


9-27-21
Objective: Design Doc Check

 - Attended design doc check

 - Feedback: scale back thermal unit smaller range of temperatures to be tested

9-28-21
Objective: PCB Review

 - Attended PCB Review

 - Feedback: separate power supply into high power and low power power supplies

 - Low Power Supply AZ1117C-5.0 regulator selected for stability
DATASHEET(https://www.diodes.com/assets/Datasheets/AZ1084C.pdf)

 - High Power Supply MIC29751-5.0WT regulator selected for max current of 7.5 A
DATASHEET(https://rocelec.widen.net/view/pdf/cqkinzilfg/MCRLS04602-1.pdf?t.download=true&u=5oefqw)


9-20-21
Objective: Submit Design Doc

 - Incorportated design doc review feedback and PCB review feedback into design doc

 - Revised block diagram (see revamped block diagram (1).png)

 - Put finishing touches on design doc


10-1-21
Objective: Meet With TA (Stasiu)

 - Discussed cost of solid state relays with Stasiu. Decided to move to mechanical relays as they are far less expensive.


10-6-21
Objective: Design Review

 - Attended Design Review

 - Feedback: Shrink volume of thermal unit, more safety considerations for discharging lithium ion cells (procedure for performing live tests)

 - Stasiu recommends shrinking trace width to accomodate charging IC

10-7-21
Objective: Complete PCB Layout and Submit to TA (Stasiu)

 - Width between pins on charging IC too small for standard PCBway manufacturing tolerances (total width 4 mil)

 - It is impossible to use our chosen charging IC with even PCBway's advanced manufacturing tolerances

 - Tried removing charging IC to get partially functional board manufactured.

 - Revised board still requires advanced manufacturing tolerances. Course will not purchase the board.


10-8-21
Objective: Work on Physical Design

 -Investigated thermal chamber and cooling requirements

 -settled on x2 30x30mm peltier devices. Each produce around 9.19 watts of heat and pump around 8.50(w)
DATASHEET(https://www.cuidevices.com/product/resource/cp36h-2.pdf)

 -settled on Wakefield-Vette 641A heatsink for hot side of both peltiers. Will run at about 30C radiating ~20 watts. This requires forced airflow of 250LFM. 
DATASHEET(http://www.wakefield-vette.com/Portals/0/resources/datasheets/641.pdf)

 -settled on APF30-30-13CB for cold side of peltier devices. Will also require forced airflow (200LFM), adding around 2 watts to the internal temperature of the chamber.
DATASHEET(https://www.ctscorp.com/wp-content/uploads/Plate-Fin-Configuration-Oct2018.pdf)

 -settled on fans to provide adequate airflow (300 LFM each exceeds required max of 250 LMF)
DATASHEET(https://media.digikey.com/pdf/Data%20Sheets/NMB-MAT/03010SS_Series.pdf)

 -final in-flux, but scale has drastically been reduced. Est. wall surface area of 0.0021 m^2.


10-13-21
Objective: Order Parts

 - Ordered large portion of parts through course website upto budget

 - Missing items recorded in 'Needto Order.txt'

10-14-21
Objective: Sort out charging IC

 - All charging ICs avaible from Digikey that can supply 5 A constant charging current use the same package as the failed charging IC

 - Revised current requirements to 3A to increase number of availble parts

 - Chose MAX77975 Li+ charger for constant current capabilities
DATASHEET(https://datasheets.maximintegrated.com/en/ds/MAX77975-MAX77976.pdf#page7)

10-15-21
Objective: Meet with TA (Stasiu)

 - Met with Stasiu discussed status of project


10-21-21
Objective: Quantify physical design

 - created a quantified blueprint (see Team 7 Physical Design Schematic png)

 - sent blueprint to Greg in machine shop

 - selected 30x30mm fans for heatsinks


10-22-21
Objective Meet with TA (Stasiu)

 - Met with Stasiu discussed status of project

10-25-21
Objective: Prepare PCB for manufacturing and work on thermal control unit

 - Thermal Control SPICE simulation performed, need to choose different components, avaible H-bridges will not work.

 - New charging IC incorporated into existing design

 - Fixed routing errors and generated GBR and DRL files

 - Attempted PCBway audit no sucess


10-28-21
Objective: Meet with Greg on Revised Thermal Unit

 - Greg recommends getting rid of beveled lid and building box interior from aluminum

 - Some fins from interior heatsinks must be removed for mounting. Not an issue as thermal unit was over designed for target temperature.

 - Design adjusted according to Greg's recommendations

 - Told that Jordan will be the machinist for our project

 - Parts given to Greg


10-29-21
Objective: Meet with TA (Stasiu) / Take thermal components to Greg in machine shop

 - Discuss thermal unit progress with Stasiu

11-3-21
Objective: Meet with Jordan in Machine Shop

 - Met with Jordan and discuss blueprints for thermal unit

 - No adjustments necessary, design remains as is (after Greg's adjustments)

 - Jordan begins working on thermal unit

11-5-21
Objective: Meet with TA (Stasiu)

 - Met with Stasiu discussed status of project


11-8-21
Objective: Main Board Corrections

 - Issues with communication protocol used for DACs found. Connected for I^2C when DACs require SPI

 - Communication protocol corrected and new boards ordered from PCBway with priority shipping


11-11-21
Objective: Assemble Thermal Board / Test Thermal Device

 - Thermal unit recieved from Jordan

 - Tested thermal unit at constant current. Small temperature decrease is followed by warming of the interior aluminum box.

 - Thermal short identified between exterior heatsink and interior metal.

 - Thermal board assembled in earnest (not tested)


11-12-21
Objective: Meet with TA (Stasiu) / Test Thermal Device

 - Asked Jordan to insulate thermal short with sheet of rubber

 - Tested thermal unit at constant current. Definitely cools, no thermometer available to quantify. Ideal cooling current found to be 2.26 A (losses weighed again cooling power)

 - Met with Stasiu and demonstrated cooling ability of thermal unit


11-17-21
Objective: Decide Main Board Issue

 - Corrected main board has still not arrived

 - Decision made to continue with incorrect main board in order to have something to demonstrate during the mock demo

 - Portions of main board assembled


11-18-21
Objective: Assemble Main Board

 - The main board assembly was completed

 - Realized that the shut down pin of all op-amps was left floating. Pin is active low, but the floating value is causing the op-amps to behave erratically.

 - Failure to communicate with ATmega32u4 on chip


11-19-21
Objective: Mock Demo

 - Not much was able to be demonstrated

 - Thermal unit demonstrated for Stasiu again

 - Nothing works properly

 - Decision made to move to broken out Arduino for control scheme

 - Moving to bread boarded op-amps as well


11-29-21
Objective: Troubleshoot Project For Final Demo

 - Thermal board made fully functional (bypassed op-amp)

 - Thermal unit tested with thermal board. Thermal board utilizes analog control signals which can be driven via power supply


11-30-21
Objective: Troubleshoot Project For Final Demo

 - Arduino board bricked

 - Completely analog control scheme adopted (digital control signals actuated via logic high and low rails on bread board)

 - obtained infrared thermal sensor from homedepot

 - Able to cool and heat thermal unit to appropriate temperatures (see Room , Cold, and Hot Temperature jpg for achieved temperatures in farenheit)

 - Able to draw a current through the discharge circuitry. DAC bypassed, buffered potentiometer provided input current. Current drawn from AAA battery stack. Relatively controllable current with excessive noise produced. Max unsustained current of ~3 A


12-1-21
Objective: Final Demo / Prepare for Mock Presentation

 - Presented final demo

 - Met up and prepared slides for mock presentation and rehearsed


12-2-21
Objective: Mock Presentation

 - Presented mock presentation

 - Feedback: less filler words and more images in slides


12-4-21
Objective: Perform Additional Thermal Testing

 - Performed additional thermal tests using infrared thermal sensor

 - 3 cold runs at 1 A, 2 A, and 3 A constant current respectively. Temperature recorded every 2 minutes

 - 2 A run reached the coldest temperature (~15 C) supporting work done identifying 2.16 A as optimal cooling current

 - 1 hot run at 2 A. No need for additional runs. Heat exceeded target of 35 C by ~20 C.


12-5-21
Objective: Prepare for Final Presentation

 - Met to rehearse final presentation

 - Reformatted of slides changed to offical template


12-6-21
Objective: Prepare for Final Presentation

 - Met and rehearsed final version of slides

 - Worked on reducing presentation length to 25 mins


12-7-21
Objective: Give Final Presentation

- Attended and gave final presentation


12-8-21
Objective: Write Final Report

- Wrote final report

