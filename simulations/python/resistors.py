import numpy as np
import matplotlib.pyplot as plt

R0 = 1000
NUM_RES0 = 64
R1 = 20
NUM_RES1 = 32

def iv_curve(voltage, num_res, res):

    current = np.zeros(num_res)
    for i in range(num_res):

        current[i] = (i+1) * voltage / res

    return current


if __name__=="__main__":

    voltage = np.linspace(2.0, 5.0, 7)
    for i in range(len(voltage)):
        plt.plot(np.arange(1, NUM_RES0+1), iv_curve(voltage[i], NUM_RES0, R0), 'o', label=str(2.0+0.5*i)+'V - 0')
        plt.plot(np.arange(1, NUM_RES1+1), iv_curve(voltage[i], NUM_RES1, R1), 'o', label=str(2.0+0.5*i)+'V - 1')

    plt.legend()
    plt.show()