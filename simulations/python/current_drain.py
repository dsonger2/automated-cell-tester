import numpy as np
import matplotlib.pyplot as plt


def bjt_sim():
    low_current = "low_current.txt"
    high_current = "high_current.txt"

    data_low = np.loadtxt(low_current, delimiter='\t', skiprows=1)
    data_high = np.loadtxt(high_current, delimiter='\t', skiprows=1)

    vIn = data_low[:, 0]
    iC_low = data_low[:, 2]
    iC_high = data_high[:, 2]

    plt.plot(vIn, iC_low, 'k-')
    # plt.plot(vIn, iC_high, 'k-')
    plt.grid()
    plt.xlabel('Input Voltage (V)')
    plt.ylabel('BJT Collector Current (A)')
    # plt.legend(['Low current mode', 'High current mode'])
    plt.title('Low current mode')
    plt.show()


if __name__=="__main__":

    bjt_sim()