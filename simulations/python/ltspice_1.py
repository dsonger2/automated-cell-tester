import numpy as np
import matplotlib.pyplot as plt
from battery_drain import plot_2n2222

def read_this(filename_in, filename_out):

    file_in = open(filename_in, 'r', encoding="utf-8")
    file_out = open(filename_out, 'w', encoding="utf-8")
    line = file_in.readline()
    print(line)
    next = True
    while(next):
        next, line = hasNext(file_in)
        line = line.replace('d', '')
        line = line.replace('B','')
        line = line.replace(u'\N{DEGREE SIGN}', '')
        line = line.replace('(', '')
        line = line.replace(')', '')
        line = line.replace(',', '\t')
        file_out.write(line)

    file_in.closed
    file_out.closed

def hasNext(file):
    line = file.readline()
    if line != "":
        return True, line
    else:
        return False, line

if __name__=="__main__":

    source = "battery_drain_sim.txt"
    target = "dontread.txt"
    final = "usable.txt"
    new = True
    special = False

    if new:
        if special:
            with open(source, "r", encoding="utf-8") as sourceFile:
                with open(target, "w", encoding="utf-8") as targetFile:
                    line = sourceFile.readline()
                    newline = ""
                    for i in range(len(line)):
                        print(line[i:i+3])
                        if line[i:i+3] != "\x00":
                            newline += line[i]
                        else:
                            print("here")
                            i += 3
                    targetFile.write(newline)
        BLOCKSIZE = 1048576  # or some other, desired size in bytes
        with open(source, "r", encoding="ISO-8859-1") as sourceFile:
            with open(target, "w", encoding="utf-8") as targetFile:
                while True:
                    contents = sourceFile.read(BLOCKSIZE)
                    if not contents:
                        break
                    targetFile.write(contents)

        read_this(target, final)

    # Handle the data
    data = np.loadtxt(source, delimiter='\t', skiprows=1)

    plot_2n2222(data)

