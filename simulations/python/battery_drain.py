import numpy as np
import matplotlib.pyplot as plt

def plot_2n2222(data):

    vDAC = np.array(data[:, 0])
    vADC = np.array(data[:, 1])
    iC = np.array(data[:, 2])

    plt.plot(vDAC, vADC)
    plt.grid()
    plt.xlabel('Base voltage (V)')
    plt.ylabel('Shunt sensor voltage (V)')
    plt.title('ADC input vs. DAC output')

    plt.show()

    plt.plot(vDAC, iC)
    plt.grid()
    plt.xlabel('Base voltage (V)')
    plt.ylabel('Current (A)')
    plt.title('Amperage across BJT vs. DAC output')

    plt.show()
