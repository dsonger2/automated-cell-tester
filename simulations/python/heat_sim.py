import numpy as np
import matplotlib.pyplot as plt

DIFFUSION_COEFF = 0.45
GRANULARITY = 0.01

def distance_calc(atom_pos, pelt_pos):
    dist_mat = np.zeros([2, 3])
    for i in range(len(pelt_pos)):
        dist = np.sqrt((pelt_pos[i][0] - atom_pos[0])**2 + (.5 - atom_pos[1])**2 + (.5 - atom_pos[2])**2)
        # dist = np.sqrt((pelt_pos[i][0] - atom_pos[0])**2 + (atom_pos[1])**2 + (pelt_pos[i][1] - atom_pos[2])**2)
        dist_mat[int(i / int(3))][i % 3] = dist

    return dist_mat

def heat_calc(dist_mat, pelt_pwr):

    differential = 0.0

    weight_mat = DIFFUSION_COEFF * dist_mat @ np.array([[pelt_pwr[0], pelt_pwr[1]], [pelt_pwr[2], pelt_pwr[3]], [pelt_pwr[4], pelt_pwr[5]]])
    for i in range(len(weight_mat)):
        for j in range(len(weight_mat[0])):
            differential += weight_mat[i, j]

    return differential

def average_heat(x, y, field):

    averages = np.ones((len(x), len(y)))

    for i in range(len(x)):
        for j in range(len(y)):
            averages[i, j] = 1.0 - np.average(field[i, j]) / 22.5
    return averages

def simulation(num_timesteps, gridsize):

    pelt_size = (gridsize - 4.0 * 0.05 * gridsize) / 3.0
    loc0 = .05*gridsize + pelt_size/2
    loc1 = 2 * loc0
    loc2 = 3 * loc0

    field = np.zeros((gridsize, gridsize, gridsize))
    peltier_positions = GRANULARITY * np.array([[loc0, loc0], [loc0, loc1], [loc0, loc2], [loc1, loc0], [loc1, loc1], [loc1, loc2]])
    peltier_pwr = 0.9 * 5.0 * 1.5 / 2.0
    pwr_mat = peltier_pwr * np.ones(6)

    for i in range(len(field)):
        for j in range(len(field[0])):
            for k in range(len(field[0, 0])):
                pos = GRANULARITY * np.array([i, j, k])
                dist = distance_calc(pos, peltier_positions)
                field[i, j, k] = heat_calc(dist, pwr_mat)


    x = GRANULARITY * np.arange(gridsize)
    y = GRANULARITY * np.arange(gridsize)

    X, Y = np.meshgrid(x, y)
    temp_avgs = average_heat(X, Y, field)

    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, temp_avgs, rstride=1, cstride=1,
                    cmap='viridis', edgecolor='none')
    ax.set_title('Normalized visualization of heat dissipation')
    ax.set_xlabel('x-position')
    ax.set_ylabel('y-position')
    ax.set_zlabel('Thermal effect')
    plt.show()


if __name__=="__main__":

    simulation(0, 100)
