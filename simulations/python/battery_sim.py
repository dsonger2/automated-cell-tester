import numpy as np
import matplotlib.pyplot as plt

INT_RES = 0.002
V_INIT = 3.6
CAP_COEFF = 0.0001

SIM_LENGTH = 3

MINUTE = 60
HOUR = 3600

class DataPoint:
    def __init__(self, t, v, i):
        self.v = v
        self.i = i
        self.t = t


    def to_string(self):
        return "{}\t{:.4f}\t{}\n".format(self.t, self.v, self.i)


def simulate_timestep(t):
    time = t % HOUR         # every hour
    if time <= (10*MINUTE):
        i = 5.0
    elif time <= (30*MINUTE):
        i = -0.1
    else:
        i = 0.0

    return i


if __name__=="__main__":

    filename = "batt_sim.txt"
    curr_v = V_INIT
    curr_ocv = V_INIT
    curr_i = 0.0

    with open(filename, "w", encoding="utf-8") as out_file:
        for hour in range(SIM_LENGTH):
            for time in range(HOUR):

                curr_i = simulate_timestep(time)
                curr_ocv = curr_ocv - CAP_COEFF*curr_i
                curr_v = curr_ocv - INT_RES*curr_i
                dp = DataPoint(3600*hour + time, curr_v, curr_i)
                out_file.write(dp.to_string())

    data = np.loadtxt(filename, delimiter='\t', skiprows=1)

    t = np.array(data[:,0])
    v = np.array(data[:,1])
    i = np.array(data[:,2])

    plt.plot(t, v, 'k', label="Voltage (V)")
    plt.xlabel('time (s)')
    plt.ylabel('Voltage (V)')
    plt.show()

    plt.plot(t, i, 'g', label="Current (A)")
    plt.ylabel('Amperage (A)')
    plt.xlabel('time (s)')
    plt.show()