#include <Wire.h>
#include "utilities.h"

// sensor buffers
int ampbuffer_cntr = 0;
int ampbuffer[20];
int voltbuffer_cntr = 0;
int voltbuffer[20];
int thermbuffer_cntr = 0;
int thermbuffer[20];

// measurements
int current = 4095 >> 1;
int voltage = 4095 >> 1;
int temperature = 4095 >> 1;

// test state variables
int charge_ready = 0;
int discharge_ready = 0;
int amperage_req = 0;
int state = 0;

byte error_state = 0;

unsigned long timestep = 0;

float data[][3] = {{0.0, 3.5895, 5.0},{1.0, 3.589, 5.0},{2.0, 3.5885, 5.0},{3.0, 3.588, 5.0},{4.0, 3.5875, 5.0},{5.0, 3.587, 5.0},{6.0, 3.5865, 5.0},{7.0, 3.586, 5.0},{8.0, 3.5855, 5.0},{9.0, 3.585, 5.0},{10.0, 3.5845, 5.0},{11.0, 3.584, 5.0},{12.0, 3.5835, 5.0},{13.0, 3.583, 5.0},{14.0, 3.5825, 5.0},{15.0, 3.582, 5.0},{16.0, 3.5815, 5.0},{17.0, 3.581, 5.0},{18.0, 3.5805, 5.0},{19.0, 3.58, 5.0},{20.0, 3.5795, 5.0},{21.0, 3.579, 5.0},{22.0, 3.5785, 5.0},{23.0, 3.578, 5.0},{24.0, 3.5775, 5.0},{25.0, 3.577, 5.0},{26.0, 3.5765, 5.0},{27.0, 3.576, 5.0},{28.0, 3.5755, 5.0},{29.0, 3.575, 5.0},{30.0, 3.5745, 5.0},{31.0, 3.574, 5.0},{32.0, 3.5735, 5.0},{33.0, 3.573, 5.0},{34.0, 3.5725, 5.0},{35.0, 3.572, 5.0},{36.0, 3.5715, 5.0},{37.0, 3.571, 5.0},{38.0, 3.5705, 5.0},{39.0, 3.57, 5.0},{40.0, 3.5695, 5.0},{41.0, 3.569, 5.0},{42.0, 3.5685, 5.0},{43.0, 3.568, 5.0},{44.0, 3.5675, 5.0},{45.0, 3.567, 5.0},{46.0, 3.5665, 5.0},{47.0, 3.566, 5.0},{48.0, 3.5655, 5.0},{49.0, 3.565, 5.0},{50.0, 3.5645, 5.0},{51.0, 3.564, 5.0},{52.0, 3.5635, 5.0},{53.0, 3.563, 5.0},{54.0, 3.5625, 5.0},{55.0, 3.562, 5.0},{56.0, 3.5615, 5.0},{57.0, 3.561, 5.0},{58.0, 3.5605, 5.0},{59.0, 3.56, 5.0},{60.0, 3.5595, 5.0},{61.0, 3.559, 5.0},{62.0, 3.5585, 5.0},{63.0, 3.558, 5.0},{64.0, 3.5575, 5.0},{65.0, 3.557, 5.0},{66.0, 3.5565, 5.0},{67.0, 3.556, 5.0},{68.0, 3.5555, 5.0},{69.0, 3.555, 5.0},{70.0, 3.5545, 5.0},{71.0, 3.554, 5.0},{72.0, 3.5535, 5.0},{73.0, 3.553, 5.0},{74.0, 3.5525, 5.0},{75.0, 3.552, 5.0},{76.0, 3.5515, 5.0},{77.0, 3.551, 5.0},{78.0, 3.5505, 5.0},{79.0, 3.55, 5.0},{80.0, 3.5495, 5.0},{81.0, 3.549, 5.0},{82.0, 3.5485, 5.0},{83.0, 3.548, 5.0},{84.0, 3.5475, 5.0},{85.0, 3.547, 5.0},{86.0, 3.5465, 5.0},{87.0, 3.546, 5.0},{88.0, 3.5455, 5.0},{89.0, 3.545, 5.0},{90.0, 3.5445, 5.0},{91.0, 3.544, 5.0},{92.0, 3.5435, 5.0},{93.0, 3.543, 5.0},{94.0, 3.5425, 5.0},{95.0, 3.542, 5.0},{96.0, 3.5415, 5.0},{97.0, 3.541, 5.0},{98.0, 3.5405, 5.0},{99.0, 3.54, 5.0}};

byte data_reg[12];

//Test *test;
byte test_req;
byte test_active;

unsigned int timer = 0;


void usb_write(byte type, unsigned long timestep, int volts, int curr, int temp)
{
  // message writer driver
  data_reg[0] = type;
  data_reg[1] = (byte)((timestep >> 24) & 0xFF);
  data_reg[2] = (byte)((timestep >> 16) & 0xFF);
  data_reg[3] = (byte)((timestep >> 8) & 0xFF);
  data_reg[4] = (byte)((timestep) & 0xFF);
  data_reg[5] = (byte)((volts >> 8) & 0xFF);
  data_reg[6] = (byte)((volts) & 0xFF);
  data_reg[7] = (byte)((curr >> 8) & 0xFF);
  data_reg[8] = (byte)((curr) & 0xFF);
  data_reg[9] = (byte)((temp >> 8) & 0xFF);
  data_reg[10] = (byte)((temp) & 0xFF);
  Serial.write(data_reg, 11);
}

void handle_serial() 
{
  test_req = 0x00;
  byte mesg_type = Serial.read() & 0x07;

  if (mesg_type == 0x0A) { return; }

  switch (mesg_type)
  {
    case WARM_MESG:
      update_temperature(HOT);
      break;
     case COOL_MESG:
      update_temperature(COLD);
      break;
     case DISCHARGE_MESG:
      digitalWrite(DISCHARGE_READY_PIN, HIGH);
      update_discharger(1);
      break;
     case STOP_MESG:
     default:
      digitalWrite(DISCHARGE_READY_PIN, LOW);
      update_discharger(0);
      break;
  }

//  if (mesg_type == (byte) START_TEST_MESG) {
//    test_req |= Serial.read();
//    // update_temperature() shouldn't return until the temperature is ready.
//    test->set_test((byte) test_req, millis());
//    test_active = 0xFF;
//    digitalWrite(DEBUG_PIN, HIGH);
//  } else if (mesg_type == (byte) STOP_TEST_MESG) {
//    test->update_test(millis(), STOP);
////    Serial.println("STOP TEST");
////    Serial.println(mesg_type);
//    digitalWrite(DEBUG_PIN, LOW);
//    test_active = 0x00;
//  }
}

void safety_stop()
{

  noInterrupts();

  // TODO: Must ask for 0A first?

  digitalWrite(CHARGE_READY_PIN, LOW);
  charge_ready = 0;
  digitalWrite(DISCHARGE_READY_PIN, LOW);
  discharge_ready = 0;

  // Create data and write it over bluetooth
  timestep = (unsigned long) (millis()); // - test->test_start);
  usb_write(ERROR_MESG, timestep, voltage, current, temperature);

  interrupts();
  
}

void setup()
{


  pinMode(DEBUG_PIN, OUTPUT);
  digitalWrite(DEBUG_PIN, LOW);
  
  pinMode(CHRG_IRQ_PIN, INPUT);
  pinMode(CHARGE_READY_PIN, OUTPUT);
  pinMode(DISCHARGE_READY_PIN, OUTPUT);
  pinMode(THERM1_PIN, INPUT);
  

  // initialize timer1 

//  noInterrupts();           // disable all interrupts
//
//  TCCR1A = 0;
//  TCCR1B = 0;
//  TCNT1  = 0;
//
//
//  OCR1A = 6250;            // compare match register 16MHz/256/2Hz
//  TCCR1B |= (1 << WGM12);   // CTC mode
//  TCCR1B |= (1 << CS12);    // 256 prescaler 
//  TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt
//
//  interrupts();             // enable all interrupts

  Wire.begin();
  Serial.begin(115200);

  // Arduino setup
  data_reg[0] = (byte)CURRENT_DEMAND_MESG;
  data_reg[1] = 0x00;
  data_reg[2] = 0x00;
  Wire.beginTransmission(ARDUINO_CHAN);
  Wire.write(data_reg, 3);
  Wire.endTransmission();

  // Analog pin setup

  // IC setup
  data_reg[0] = (byte) I2C_CNFG;
  data_reg[1] = (byte) 0x10;
//  Wire.beginTransmission(IC_CHAN);
//  Wire.write(data_reg, 2);
//  Wire.endTransmission();
  // ALSO setup disable in CHG_CNFG_PWR

//  test = new Test();
  digitalWrite(DEBUG_PIN, HIGH);

}


void fake_interrupt()
{

  /* 
   *  This timer must implement:
   * 1) Coulomb counting
   * 2) Voltage read
   * 3) Thermal read
   * 4) Bluetooth communications
   */

   // Coulomb counting
   int32_t amp_sum = 0;
   
   for(int i = 0; i < 10; i++)
   {
     amp_sum += int32_t(ampbuffer[i]);
   }

  // low-pass filter in software. 
  // ampsum could probably use more manipulation than this.
  current = ( 7 * current + amp_sum / ampbuffer_cntr) >> 3;

  if(float_current(current) > OVERCURRENT_LIMIT){
    safety_stop();
  }
   
  // coulomb counter implemented here or in GUI? would need to be float math
   

  // Voltage read
  int32_t volt_sum =0;
  for(int i = 0; i < 20; i++)
  {
    volt_sum += voltbuffer[i];
  }
   
  // low-pass filter in software
  voltage = (3 * voltage + volt_sum / voltbuffer_cntr) >> 2;

  if(voltage > OVERVOLTAGE_LIMIT || voltage < UNDERVOLTAGE_LIMIT){
    safety_stop();
  }

  // Thermal read
  int32_t therm_sum = 0;
  for(int i = 0; i < 20; i++)
  {
    therm_sum += thermbuffer[i];
  }
  temperature = therm_sum / thermbuffer_cntr;

  if(temperature > HIGH_TEMP_CELL_LIMIT || temperature < LOW_TEMP_CELL_LIMIT){
    safety_stop();
  }  
  
  timer++;
  timer = timer % 100;
  temperature = int(data[timer][0]);
  voltage = int(data[timer][1] * 1000.0);
  current = int(data[timer][2] * 1000.0);
  
  timestep = (unsigned long)(millis());
//  usb_write(DATA_MESG, timestep, voltage, current, temperature);
  if (test_active) {
    usb_write((byte)DATA_MESG, timestep, voltage, current, temperature);
  } else {
    usb_write((byte)DATA_MESG, timestep, 0, 0, 0);
  }

  ampbuffer_cntr = 0;
  voltbuffer_cntr = 0;
  thermbuffer_cntr = 0;
   
}


void loop()
{

  // Handle case while waiting for test
  if (Serial.available()){
    handle_serial();
  }
  delay(10);

  timer++;
  timer = timer % 9;
  if (timer == 0)
  {
    fake_interrupt();
  }

  // Safety stop if necessary charging IC wants it.
  // This is performed externally from the interrupt so that if the charging IC detects an issue before the contoller does, we can stop charge quicker.
//  if(digitalRead(CHRG_IRQ_PIN) == HIGH){
//    safety_stop();
//  }
  
}
