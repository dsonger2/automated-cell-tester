#include <Wire.h>
#include <SPI.h>
#include "utilities.h"

// sensor buffers
int ampbuffer_cntr = 0;
int ampbuffer[20];
int voltbuffer_cntr = 0;
int voltbuffer[20];
int thermbuffer_cntr = 0;
int thermbuffer[20];

// measurements
int current = 4095 >> 1;
int voltage = 4095 >> 1;
int temperature = 4095 >> 1;

// test state variables
int charge_ready = 0;
int discharge_ready = 0;
int amperage_req = 0;
int state = 0;

char rx_char = ' ';


byte error_state = 0;

unsigned long timestep = 0;

byte data_reg[10];

Test *test;


void usb_write(byte type, unsigned long timestep, int volts, int curr, int temp)
{
  // will write over USART to bluetooth or USB if bluetooth not configured.
  byte buf[9] = {type, (byte)((timestep >> 8) & 0xFF), (byte)((timestep) & 0xFF), (byte)((volts >> 8) & 0xFF), (byte)((volts) & 0xFF), (byte)((curr >> 8) & 0xFF), (byte)((curr) & 0xFF), (byte)((temp >> 8) & 0xFF), (byte)((temp) & 0xFF)};
  Serial.write(buf, 9);
}

void safety_stop()
{

  noInterrupts();

  // TODO: Must ask for 0A first?

  digitalWrite(CHARGE_READY_PIN, LOW);
  charge_ready = 0;
  digitalWrite(DISCHARGE_READY_PIN, LOW);
  discharge_ready = 0;

  // Create data and write it over bluetooth
  timestep = (unsigned long) (millis() - test->test_start);
//  usb_write(ERROR_MESG, timestep, voltage, current, temperature);

  interrupts();
  
}

void setup()
{

  
  pinMode(CHRG_IRQ_PIN, INPUT);
  pinMode(CHARGE_READY_PIN, OUTPUT);
  pinMode(DISCHARGE_READY_PIN, OUTPUT);
  pinMode(THERM1_PIN, INPUT);
  

  // initialize timer1 

  noInterrupts();           // disable all interrupts

  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;


  OCR1A = 250;            // compare match register 16MHz/256/2Hz
  TCCR1B |= (1 << WGM12);   // CTC mode
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt

  interrupts();             // enable all interrupts

  Wire.begin();
  Serial.begin(9600);

  // Arduino setup
  data_reg[0] = (byte)CURRENT_DEMAND_MESG;
  data_reg[1] = 0x00;
  data_reg[2] = 0x00;
  Wire.beginTransmission(ARDUINO_CHAN);
  Wire.write(data_reg, 3);
  Wire.endTransmission();

  data_reg[0] = (byte)THERMAL_PWR_MESG;
  Wire.beginTransmission(ARDUINO_CHAN);
  Wire.write(data_reg, 3);
  Wire.endTransmission();

  data_reg[0] = (byte) THERMAL_CTRL_MESG;
  data_reg[1] = 0x07;
  data_reg[2] = 0xFF;
  Wire.beginTransmission(ARDUINO_CHAN);
  Wire.write(data_reg, 3);
  Wire.endTransmission();

  // IC setup
//  data_reg[0] = (byte) I2C_CNFG;
//  data_reg[1] = (byte) 0x10;
//  Wire.beginTransmission(IC_CHAN);
//  Wire.write(data_reg, 2);
//  Wire.endTransmission();
//  // ALSO setup disable in CHG_CNFG_PWR
//
//  // ADC setup
//  data_reg[0] = (byte) 0x07;
//  data_reg[1] = (byte) 0x20;
//  Wire.beginTransmission(VOLT_ADC_CHAN);
//  Wire.write(data_reg, 2);
//  Wire.endTransmission();

  test = new Test();

}


ISR(TIMER1_COMPA_vect)
{

  /* 
   *  This timer must implement:
   * 1) Coulomb counting
   * 2) Voltage read
   * 3) Thermal read
   * 4) Bluetooth communications
   */

   // Coulomb counting
   int32_t amp_sum = 0;
   
   for(int i = 0; i < 10; i++)
   {
     amp_sum += int32_t(ampbuffer[i]);
   }

  // low-pass filter in software. 
  // ampsum could probably use more manipulation than this.
  current = ( 7 * current + amp_sum / ampbuffer_cntr) >> 3;

  if(float_current(current) > OVERCURRENT_LIMIT){
    safety_stop();
  }
   
  // coulomb counter implemented here or in GUI? would need to be float math
   

  // Voltage read
  int32_t volt_sum =0;
  for(int i = 0; i < 20; i++)
  {
    volt_sum += voltbuffer[i];
  }
   
  // low-pass filter in software
  voltage = (3 * voltage + volt_sum / voltbuffer_cntr) >> 2;

  if(voltage > OVERVOLTAGE_LIMIT || voltage < UNDERVOLTAGE_LIMIT){
    safety_stop();
  }

  // Thermal read
  int32_t therm_sum = 0;
  for(int i = 0; i < 20; i++)
  {
    therm_sum += thermbuffer[i];
  }
  temperature = therm_sum / thermbuffer_cntr;

  if(temperature > HIGH_TEMP_CELL_LIMIT || temperature < LOW_TEMP_CELL_LIMIT){
    safety_stop();
  }

  timestep = (unsigned long) (millis() - test->test_start);
//  usb_write(DATA_MESG, timestep, voltage, current, temperature);

  ampbuffer_cntr = 0;
  voltbuffer_cntr = 0;
  thermbuffer_cntr = 0;
   
}


void loop()
{

  if (Serial.available())
  {
    Serial.println("MESG RX'd");
    rx_char = (byte)Serial.read();

    switch(rx_char)
    {
      case '0':
        update_ampbuffers(ampbuffer, ampbuffer_cntr);
        break;
      case '1':
        update_temperature(HOT);
        break;
      case '2':
        update_temperature(COLD);
        break;
      case '3':
        update_current_ctrl((byte)true, (byte)false, 0x003);
        break;
      default:
        Serial.print("REJECTED MESG: 0x");
        Serial.println(rx_char, HEX);
        break;
    }
  }

  delay(1000);

  // Handle case while waiting for test
//  if (test->get_current_state(voltage) == STOP) {
//    if (Serial.available()) {
//      test->set_test((unsigned int) Serial.read(), millis());
//    }
//  }
//
//  // Safety stop if necessary charging IC wants it.
//  // This is performed externally from the interrupt so that if the charging IC detects an issue before the contoller does, we can stop charge quicker.
//  if(digitalRead(CHRG_IRQ_PIN) == HIGH){
//    safety_stop();
//  }
//  
//
//  // update the ampbuffers, voltage buffers, and thermal buffers
//  update_ampbuffers(ampbuffer, ampbuffer_cntr);
//  update_voltbuffers(voltbuffer, voltbuffer_cntr);
//  update_thermbuffers(thermbuffer, thermbuffer_cntr);
  
}
