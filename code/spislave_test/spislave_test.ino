#include <SPI.h>
//#include "utilities.h"
#include "pins_arduino.h"

#define CS_PIN 53

char buf[100];
volatile byte pos;
volatile boolean process_it;

byte values[2] = {0x00, 0x00};

unsigned int timestep = 0;

void setup()
{

  Serial.begin(9600);

  pinMode(MISO, OUTPUT);
  SPCR |= _BV(SPE);
  SPCR |= _BV(SPIE);

  pos = 0;
  process_it = false;
  

//  pinMode(CS_PIN, OUTPUT);
//  SPISettings mySettings(20000000, MSBFIRST, SPI_MODE1);
  
}

// This is a comment
ISR (SPI_STC_vect)
{
byte c = SPDR;
  if(pos < sizeof (buf))
  {
    buf[pos] = c;
    pos++;
    if (c == '\n') { process_it = true; }
  }
  // Serial.print((int)c);
}

void loop()
{

  if (process_it)
  {
    buf[pos] = 0;
    Serial.println(buf);
    pos = 0;
    process_it = false;
  }

//  if (digitalRead(CS_PIN) == LOW)
//  {
//    values = SPI.transfer();
//    Serial.print("value: ");
//    Serial.print((int)((values[0] << 8) | values[1]);
//  }
//
//  delay(5);
  
}
