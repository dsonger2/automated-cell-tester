#ifndef UTILITIES_H
#define UTILITIES_H
#include <Arduino.h>

// Pins
#define CHRG_IRQ_PIN    29
#define CURRENT_MODE_PIN   43
#define DISCHARGE_READY_PIN    44
#define CHARGE_READY_PIN    45
#define THERM1_PIN    41

// Testing states
#define NO_CURRENT    0
#define DISCHARGE_3A    1
#define DISCHARGE_1A    2
#define CHARGE_3A    3
#define CHARGE_1A    4

// I2C addresses
#define CHARGE_IC_CHAN    0
#define THERMAL_DAC_CHAN    1
#define DISCHARGE_DAC_CHAN    2
#define ADC_BATT_CHAN    3
#define ADC_SHUNT_CHAN    4

// Bluetooth message types
#define CURR_MESG    0
#define VOLT_MESG    1
#define THERM_MESG 2
#define ERROR_MESG  3
#define TEST_MESG 0x04

// I2C channels
#define VOLT_ADC_CHAN               0x55
#define CURR_ADC_CHAN               0x59
#define IC_CHAN                            0x6B
#define ARDUINO_CHAN                 0x7F

// Arduino messages
#define SHUNT_READ_MESG 0
#define THERMAL_CTRL_MESG 1
#define THERMAL_PWR_MESG 2
#define CURRENT_DEMAND_MESG 3

// Current, voltage and thermal limits in integers representation
#define OVERVOLTAGE_LIMIT 0xFF0
#define UNDERVOLTAGE_LIMIT 0x7FF
#define OVERCURRENT_LIMIT 0xFF0
#define HIGH_TEMP_CELL_LIMIT 0
#define LOW_TEMP_CELL_LIMIT 0

// Sensor constants
#define VOLTAGE_FLOAT 999.0
#define VOLTAGE_INT 999
#define CURRENT_FLOAT 409.5
#define CURRENT_SENSITIVITY 5

// Feedback constants
#define MAX_AMP_DIFF 0.25
#define FEEDBACK_PROP 0.05

// Test states
#define STOP 0
#define START 1
#define DISCHARGE_1A 2
#define DISCHARGE_3A 3
#define CHARGE_1A 4
#define CHARGE_3A 5
#define HOUR_WAIT 6
#define SMALL_WAIT1 7
#define SMALL_WAIT2 8

// Test types
#define NO_TEST 0
#define CAPACITY 1
#define SOC_OCV 2
#define HPPC 3

// Time constants
#define ONE_HOUR 3600000
#define TWENTY_SECONDS 20000
#define TWENTY_MINUTES 1200000
#define THREE_MINUTES 180000

class Test
{

  public:
    unsigned int type;
    unsigned long test_start;
    Test();
  
    class TestState
    {

      public: 
        unsigned long start_time;
        unsigned int stage;
        TestState() {    stage = STOP; start_time = millis();    }
        void set_state(unsigned long new_start, unsigned int new_stage);
  
    } state;

    void set_test(unsigned int new_type, unsigned long new_start);
    void update_test(unsigned long new_start, unsigned int new_stage);
    unsigned int get_current_state(unsigned int);

  private:
    unsigned int get_capacity_state(unsigned int);
    unsigned int get_sococv_state(unsigned int, unsigned int);
    unsigned int get_hppc_state(unsigned int, unsigned int);
    
};

/*
// Preprogrammed testing values
extern unsigned int discharge_val[] = {0x00, 0x7FF, 0xFFF};
// Other important defines could be a struct or array full of DAC values?

// sensor buffers
extern int ampbuffer_cntr = 0;
extern int ampbuffer[20];
extern int voltbuffer_cntr = 0;
extern int voltbuffer[20];
extern int thermbuffer_cntr = 0;
extern int thermbuffer[20];

// measurements
extern int current = 65535 >> 1;
extern int voltage = 65535 >> 1;
extern int temperature = 65535 >> 1;

// test state variables
extern int charge_ready = 0;
extern int discharge_ready = 0;
extern int amperage_req = 0;
extern unsigned int state = 0;
*/

// INPUT
void update_ampbuffers(unsigned int[], unsigned int);
void update_voltbuffers(unsigned int[], unsigned int);
void update_thermbuffers(unsigned int[], unsigned int);

// OUTPUT
void update_current_ctrl(byte, byte, int);
void update_charger(int);
void update_discharger(int, unsigned int);
void update_discharger(int);
void update_temperature(int);

// MISC. 
void safety_stop(void);
float float_voltage(unsigned int);
float float_current(unsigned int);
float float_temperature(unsigned int);
int int_voltage(float);
int int_current(float);
int int_temperature(float);

/*
unsigned int get_current_state(Test, unsigned int);

unsigned int get_capacity_state(unsigned int, unsigned int);
unsigned int get_sococv_state(unsigned int, unsigned int, unsigned int);
unsigned int get_hppc_state(unsigned int, unsigned int, unsigned int);
*/

void setup_utilities(Test);

#endif
