#include <Wire.h>
#include "utilities.h"

// sensor buffers
int ampbuffer_cntr = 0;
int ampbuffer[20];
int voltbuffer_cntr = 0;
int voltbuffer[20];
int thermbuffer_cntr = 0;
int thermbuffer[20];

// measurements
int current = 65535 >> 1;
int voltage = 65535 >> 1;
int temperature = 65535 >> 1;

// state variables
int charge_ready = 0;
int discharge_ready = 0;
int amperage_req = 0;
int state = 0;

// Testing variables
char test_current = ' ';
int buff_cntr = 0;

Test *test;

void usb_write(unsigned long timestep, int volts, int curr, int temp)
{
  // will write over USART to bluetooth or USB if bluetooth not configured.
  byte buf[8] = {(byte)((timestep >> 8) & 0xFF), (byte)((timestep) & 0xFF), (byte)((volts >> 8) & 0xFF), (byte)((volts) & 0xFF), (byte)((curr >> 8) & 0xFF), (byte)((curr) & 0xFF), (byte)((temp >> 8) & 0xFF), (byte)((temp) & 0xFF)};
  // Serial.write(buf, 9)
  // Testing:
//  Serial.print("time: ");
//  Serial.print(timestep);
//  Serial.print("   voltage: ");
//  Serial.print(voltage);
//  Serial.print("   current: ");
//  Serial.println(current);
}

void safety_stop()
{

  noInterrupts();

  // TODO: Must ask for 0A first?

  digitalWrite(CHARGE_READY_PIN, LOW);
  charge_ready = 0;
  digitalWrite(DISCHARGE_READY_PIN, LOW);
  discharge_ready = 0;

  // Create data and write it over bluetooth
  unsigned long timestep = (unsigned long)(millis() - test->test_start);
  usb_write(timestep, voltage, current, temperature);

  interrupts();
  
}

void setup()
{

  pinMode(CHARGE_READY_PIN, OUTPUT);
  digitalWrite(CHARGE_READY_PIN, LOW);
  pinMode(DISCHARGE_READY_PIN, OUTPUT);
  digitalWrite(DISCHARGE_READY_PIN, LOW);

  // initialize timer1 

  noInterrupts();           // disable all interrupts

  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;


  OCR1A = 6250;            // compare match register 16MHz/256/2Hz
  TCCR1B |= (1 << WGM12);   // CTC mode
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt

  interrupts();             // enable all interrupts

  Wire.begin();
  Serial.begin(9600);

  // Test setup
//  update_charger(0);

  pinMode(CURRENT_MODE_PIN, OUTPUT);
  digitalWrite(CURRENT_MODE_PIN, HIGH);

  Serial.println("Finish setup");

//  Wire.beginTransmission(ARDUINO_CHAN);
//  Wire.write((byte) CURRENT_DEMAND_MESG);
//  Wire.write((byte) 0x03);
//  Wire.write((byte) 0xFF);
//  Wire.endTransmission();

  Serial.println("FINISHED RX");

}


ISR(TIMER1_COMPA_vect)
{

  /* 
   *  This timer must implement:
   * 1) Coulomb counting
   * 2) Voltage read
   * 3) Thermal read
   * 4) Bluetooth communications
   */

   // Coulomb counting
   int32_t amp_sum = 0;
   
   for(int i = 0; i < 10; i++)
   {
     amp_sum += int32_t(ampbuffer[i]);
   }

  // low-pass filter in software. 
  // ampsum could probably use more manipulation than this.
  current = ( 7 * current + amp_sum / ampbuffer_cntr) >> 3;

  if(float_current(current) > OVERCURRENT_LIMIT){
    safety_stop();
  }
   
  // coulomb counter implemented here or in GUI? would need to be float math
   

  // Voltage read
  int32_t volt_sum =0;
  for(int i = 0; i < 20; i++)
  {
    volt_sum += voltbuffer[i];
  }
   
  // low-pass filter in software
  voltage = (3 * voltage + volt_sum / voltbuffer_cntr) >> 2;

  if(voltage > OVERVOLTAGE_LIMIT || voltage < UNDERVOLTAGE_LIMIT){
    safety_stop();
  }

  unsigned long timestep = (unsigned long) (millis() - test->test_start);
  usb_write(TEST_MESG, timestep, (int)(voltage*1000.0), (int)current*1000.0), (int)(temperature*1000.0);

  ampbuffer_cntr = 0;
  voltbuffer_cntr = 0;
  thermbuffer_cntr = 0;

  test = new Test();
  test->set_test(NO_TEST, millis());

//  Serial.println("Finish ISR");
   
}


void loop()
{

//  if(Serial.available()){
//    if(Serial.read() == TEST_MESG) { test_current = Serial.read(); }
//  }

//  if(digitalRead(CHRG_IRQ_PIN) == HIGH){
//    safety_stop();
//  }

//  Serial.println("still alive");

  if (Serial.available()){
//    amperage_req = (int) (Serial.read() & 0x03);
    buff_cntr = Serial.available() - 1;
    amperage_req = 0;
    
    while(Serial.available()) { amperage_req |= (int) (Serial.read() << buff_cntr); buff_cntr--; }
    amperage_req = amperage_req & 0x0F;
    
    if (amperage_req == 0x0A) { 
      // do nothing 
    } else {
      if (true) { Serial.print("requested: 0x"); Serial.println(amperage_req, HEX); }
      if (amperage_req == 1 || amperage_req == 3) { update_current_ctrl(0xFF, 0x00, amperage_req); }
      else if (amperage_req == 4) { safety_stop(); }
      else { update_current_ctrl(0xFF, 0x00, 0x00); }
    }
  }

  // Safety stop if necessary charging IC wants it.
  // This is performed externally from the interrupt so that if the charging IC detects an issue before the contoller does, we can stop charge quicker.
//  if(digitalRead(CHRG_IRQ_PIN) == HIGH){
//    safety_stop();
//  }
  

  // update the ampbuffers, voltage buffers, and thermal buffers
//  update_ampbuffers(ampbuffer, ampbuffer_cntr);
//  update_voltbuffers(voltbuffer, voltbuffer_cntr);
//  update_thermbuffers(thermbuffer, thermbuffer_cntr);
  
  // control the chargers and dischargers. 
//  if (discharge_ready) {
//    digitalWrite(DISCHARGE_READY_PIN, HIGH);
//  } else {
//    digitalWrite(DISCHARGE_READY_PIN, LOW);
//    digitalWrite(CHARGE_READY_PIN, LOW);
//  }
  
//  update_discharger(amperage_req, current);
  
}
