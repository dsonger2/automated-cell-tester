#include "utilities.h"
#include <Wire.h>
#include <SPI.h>

// Preprogrammed testing values
unsigned int discharge_val[] = {0x00, 0x7FF, 0xFFF};

byte data_reg[2] = {0x00, 0x00};

void update_ampbuffers(unsigned int ampbuffers[], unsigned int ampbuffer_cntr)
{

  unsigned int num_bytes = 0;
  unsigned int amperage = 0;

  Wire.requestFrom(CURR_ADC_CHAN, 2);    // request 2 bytes from slave device the current sensor ADC

  while(Wire.available())    // slave may send less than requested
  {
    amperage = (amperage << (8*num_bytes)) + Wire.read();    // receive a bytes FIFO
    // amperage = amperage + (Wire.read() << (8*num_bytes));    // receive bytes LIFO
    num_bytes++;
  }



  if(num_bytes == 1){
    if(amperage > 4095){
      ampbuffers[ampbuffer_cntr] = amperage;
      ampbuffer_cntr++;
    }
  }

}

void update_voltbuffers(unsigned int voltbuffers[], unsigned int voltbuffer_cntr)
{
  
  unsigned int num_bytes = 0;
  unsigned int voltage = 0;

  Wire.requestFrom(VOLT_ADC_CHAN, 2);    // request 6 bytes from slave device #2

  while(Wire.available())    // slave may send less than requested
  {
    voltage = (voltage << (8*num_bytes)) + Wire.read();    // receive a bytes FIFO
    // voltage = voltage + (Wire.read() << (8*num_bytes));    // receive bytes LIFO
    num_bytes++;
  }

  if(num_bytes == 1){
    voltbuffers[voltbuffer_cntr] = voltage;
    voltbuffer_cntr++;
  }

}

void update_thermbuffers(unsigned int thermbuffer[], unsigned int thermbuffer_cntr)
{
  
  // Thermal chamber thermistor (analog input)
  unsigned int temperature = analogRead(THERM1_PIN);

  thermbuffer[thermbuffer_cntr] = temperature;
  thermbuffer_cntr++;

  

}

void update_charger(int amperage_req)
{

  Wire.beginTransmission(IC_CHAN);  // device address is specified in datasheet
  Wire.write(byte(0x00));                   // sends instruction byte  
  switch(amperage_req)
  {
    case -3:
      Wire.write(0xFFF);
      break;
    case -1:
      Wire.write(0x555);
      break;
    case 0:
      Wire.write(0x000);
      break;
    case 1:
      Wire.write(0x000);
      break;
    case 3:
      Wire.write(0x000);
      break;
    default:
      Wire.write(0x00);
      break;
  }
  Wire.write(0x00);                               // sends potentiometer value byte  
  Wire.endTransmission();   
    
}

void update_discharger(int amperage_req, unsigned int current)
{
  
  if(amperage_req < 0){
    return;
  }

  // Proportionality control (negative feedback)
  float temp_current = float_current(current);
  int new_demand = 0;
  
  if(abs(float(amperage_req) - temp_current) < MAX_AMP_DIFF){
    temp_current -= FEEDBACK_PROP * (temp_current - float(amperage_req));
    new_demand = int_current(temp_current);
  } else {
    switch(amperage_req)
    {
      case 3:
        new_demand = discharge_val[2];
        break;
      case 1:
        new_demand = discharge_val[1];
        break;
      case 0:
        new_demand = discharge_val[0];
        break;
      default:
        new_demand = discharge_val[0];
        break;
    }
  }

  data_reg[0] = (byte)(0xC0 | ((new_demand >> 8) & 0x000F));
  data_reg[1] = (byte)(new_demand & 0xFF);
  digitalWrite(DISCHARGE_DAC_CS, LOW);
  SPI.transfer(data_reg, 2);
  digitalWrite(DISCHARGE_DAC_CS, HIGH);
  
}

void update_temperature(int therm_req)
{

  unsigned int current_req = THERMAL_PWR_OFF;
  unsigned int temp = 0x00;
  byte data_reg2[2] = {0x00, 0x00};

  Serial.print("req : ");
  Serial.println(therm_req);

  if (therm_req == HOT) {
    current_req = THERMAL_PWR_ON;  // 4V (current)
    digitalWrite(THERMAL_CTRL_PIN, HIGH);
    Serial.println("HEATING");
  } else if (therm_req == COLD) {
    current_req = THERMAL_PWR_ON; // 4V (current)
    digitalWrite(THERMAL_CTRL_PIN, LOW);
    Serial.println("COOLING");
  }

  Serial.print("pwr = ");
  Serial.println(current_req);
  Serial.print("temp = ");
  Serial.println(temp);

  // Write to analog
  analogWrite(THERMAL_PWR_PIN, current_req);
//  digitalWrite(THERMAL_CTRL_PIN, temp);

}

float float_voltage(unsigned int voltage) { return float(voltage) / 999.0; }

float float_current(unsigned int current)
{

  // return float(current<<1) / 409.5 - 10;  // Set for -10A to 10A
  return float(current) / CURRENT_FLOAT - float(CURRENT_SENSITIVITY);  // Set for -5A to 5A.
  
}

float float_temperature(unsigned int temperature)
{

  float factor = -1.0; // division factor for turning voltage into temperature
  return -1.0 * float(temperature) / factor;
  
}

int int_voltage(float voltage) {  return VOLTAGE_INT * int(voltage); }

int int_current(float current)
{

  return int( CURRENT_FLOAT * (current + CURRENT_SENSITIVITY));
  
}

Test::Test()
{
  state = TestState();
  type = NO_TEST;
}

void Test::TestState::set_state(unsigned long new_start, unsigned int new_stage)
{

  start_time = new_start;
  stage = new_stage;
  
}
void Test::set_test(unsigned int new_type, unsigned long new_start)
{

  type = new_type;
  state.set_state(new_start, START);
  
}

void Test::update_test(unsigned long new_start, unsigned int new_stage)
{
  state.set_state(new_start, new_stage);
}


unsigned int Test::get_current_state(unsigned int voltage)
{
  unsigned int prev_state = state.stage;
  switch(type)
  {
    case CAPACITY:
      return this->get_capacity_state(voltage);
      break;
    case SOC_OCV:
      return this->get_sococv_state(millis(), voltage);
      break;
    case HPPC:
      return this->get_hppc_state(millis(), voltage);
      break;
    default:
      return STOP;
      break;
  }

  return STOP;
  
}

unsigned int Test::get_capacity_state(unsigned int voltage)
{
  unsigned int prev_state = state.stage;
  if (prev_state == START) {
    this->update_test(millis(), DISCHARGE_1A);
    return DISCHARGE_1A;
  } else if (voltage < LOW_TEMP_CELL_LIMIT) {
    this->update_test(millis(), STOP);
    return STOP;
  }

  return prev_state;
  
}

unsigned int Test::get_sococv_state(unsigned int curr_time, unsigned int voltage)
{
  unsigned int prev_state = state.stage;
  if (prev_state == START) {
    this->update_test(curr_time, DISCHARGE_1A);
    return DISCHARGE_1A;
  } else if (prev_state == DISCHARGE_1A && voltage < LOW_TEMP_CELL_LIMIT) {
    this->update_test(curr_time, HOUR_WAIT);
    return HOUR_WAIT;
  } else if (prev_state == HOUR_WAIT && ((curr_time - this->state.start_time) > ONE_HOUR)) {
    this->update_test(curr_time, CHARGE_1A);
    return CHARGE_1A;
  } else if (prev_state == CHARGE_1A && voltage > HIGH_TEMP_CELL_LIMIT) {
    this->update_test(curr_time, STOP);
    return STOP;
  } 

  return prev_state;
  
}

unsigned int Test::get_hppc_state(unsigned int curr_time, unsigned int voltage)
{
  unsigned int prev_state = state.stage;
  if (prev_state == START) {
    this->update_test(curr_time, HOUR_WAIT);
    return HOUR_WAIT;
  } else if (prev_state == HOUR_WAIT && ((curr_time - this->state.start_time) > ONE_HOUR)) {
    this->update_test(curr_time, DISCHARGE_3A);
    return DISCHARGE_3A;
  } else if (prev_state == DISCHARGE_3A && voltage < LOW_TEMP_CELL_LIMIT) {
    // Unsure if this state is logically correct
    this->update_test(curr_time, STOP);
    return STOP;
  } else if (prev_state == DISCHARGE_3A && ((curr_time - this->state.start_time) > TWENTY_SECONDS)) {
    this->update_test(curr_time, SMALL_WAIT1);
    return SMALL_WAIT1;
  } else if (prev_state == SMALL_WAIT1 && ((curr_time - this->state.start_time) > THREE_MINUTES)) {
    this->update_test(curr_time, CHARGE_3A);
    return CHARGE_3A;
  } else if (prev_state == CHARGE_3A && ((curr_time - this->state.start_time) > TWENTY_SECONDS)) {
    this->update_test(curr_time, SMALL_WAIT2);
    return SMALL_WAIT2;
  } else if (prev_state == SMALL_WAIT2 && ((curr_time - this->state.start_time) > THREE_MINUTES)) {
    this->update_test(curr_time, DISCHARGE_1A);
    return DISCHARGE_1A;
  } else if (prev_state == DISCHARGE_1A && ((curr_time - this->state.start_time) > TWENTY_MINUTES)) {
    this->update_test(curr_time, HOUR_WAIT);
    return HOUR_WAIT;
  }

  return prev_state;
  
}
