#ifndef UTILITIES_H
#define UTILITIES_H
#include <Arduino.h>

// Pins
#define CHRG_IRQ_PIN    29
#define CURRENT_MODE_PIN   43
#define DISCHARGE_READY_PIN   3
#define CHARGE_READY_PIN   4
#define THERM1_PIN    41

// Testing states
#define NO_CURRENT    0
#define DISCHARGE_3A    1
#define DISCHARGE_1A    2
#define CHARGE_3A    3
#define CHARGE_1A    4

// USB message types
#define DATA_MESG 0x00
#define ERROR_MESG  0x01
#define START_TEST_MESG 0x02
#define STOP_TEST_MESG 0x03

// I2C channels
#define VOLT_ADC_CHAN               0x55
#define CURR_ADC_CHAN               0x59
#define IC_CHAN                            0x6B

// Charging IC specific registers
#define CHG_CNFG_PWR 0x16
#define CHG_CNFG_CV 0x17
#define CHG_CNFG_CC 0x18
#define I2C_CNFG 0x40

// Special register calues for PWR
#define CHARGER_ON 0x5
#define CHARGER_OFF 0x0

// SPI pins
#define DISCHARGE_DAC_CS      0
#define THERM_DAC_CS            0

// Thermal testing defines
#define NONE 0
#define HOT 1
#define COLD 2

#define THERMAL_PWR_ON 0xFF
#define THERMAL_PWR_OFF 0
#define THERMAL_CTRL_HOT 0xFF
#define THERMAL_CTRL_COLD 0x00
#define THERMAL_CTRL_NONE 0x7F

#define THERMAL_PWR_PIN A2
#define THERMAL_CTRL_PIN A3

// Current, voltage and thermal limits in integers representation
#define OVERVOLTAGE_LIMIT 0
#define UNDERVOLTAGE_LIMIT 0
#define OVERCURRENT_LIMIT 0
#define HIGH_TEMP_CELL_LIMIT 0
#define LOW_TEMP_CELL_LIMIT 0

// Sensor constants
#define VOLTAGE_FLOAT 999.0
#define VOLTAGE_INT 999
#define CURRENT_FLOAT 409.5
#define CURRENT_SENSITIVITY 5

// Feedback constants
#define MAX_AMP_DIFF 0.25
#define FEEDBACK_PROP 0.05

// Test states
#define STOP 0
#define START 1
#define DISCHARGE_1A 2
#define DISCHARGE_3A 3
#define CHARGE_1A 4
#define CHARGE_3A 5
#define HOUR_WAIT 6
#define SMALL_WAIT1 7
#define SMALL_WAIT2 8



// Test types
#define NO_TEST 0
#define CAPACITY 1
#define SOC_OCV 2
#define HPPC 3

// Time constants
#define ONE_HOUR 3600000
#define TWENTY_SECONDS 20000
#define TWENTY_MINUTES 1200000
#define THREE_MINUTES 180000

class Test
{

  public:
    unsigned int type;
    unsigned int temp;
    unsigned long test_start;
    Test();
  
    class TestState
    {

      public: 
        unsigned long start_time;
        unsigned int stage;
        TestState() {    stage = STOP; start_time = millis();    }
        void set_state(unsigned long new_start, unsigned int new_stage);
  
    } state;

    void set_test(byte new_type, unsigned long new_start);
    void update_test(unsigned long new_start, unsigned int new_stage);
    unsigned int get_current_state(unsigned int);

  private:
    unsigned int get_capacity_state(unsigned int);
    unsigned int get_sococv_state(unsigned int, unsigned int);
    unsigned int get_hppc_state(unsigned int, unsigned int);
    
};

// INPUT
void update_ampbuffers(unsigned int[], unsigned int);
void update_voltbuffers(unsigned int[], unsigned int);
void update_thermbuffers(unsigned int[], unsigned int);

// OUTPUT
void update_charger(int);
void update_discharger(int, unsigned int);
void update_temperature(int);

// MISC. 
void safety_stop(void);
float float_voltage(unsigned int);
float float_current(unsigned int);
float float_temperature(unsigned int);
int int_voltage(float);
int int_current(float);
int int_temperature(float);

/*
unsigned int get_current_state(Test, unsigned int);

unsigned int get_capacity_state(unsigned int, unsigned int);
unsigned int get_sococv_state(unsigned int, unsigned int, unsigned int);
unsigned int get_hppc_state(unsigned int, unsigned int, unsigned int);
*/

void setup_utilities(Test);

#endif
