#include <SPI.h>
#include <Wire.h>

#define SHUNT_POS_PIN A1
#define SHUNT_NEG_PIN A2
#define THERMAL_CTRL_PIN A3
#define THERMAL_PWR_PIN A4
#define CURRENT_MODE_PIN A5
#define CURRENT_DEMAND_PIN A0

#define SHUNT_READ_MESG 0
#define THERMAL_CTRL_MESG 1
#define THERMAL_PWR_MESG 2
#define CURRENT_DEMAND_MESG 3
//#include "pins_arduino.h"

#define CS_PIN 1
#define ONE_AMP 0x155
#define THREE_AMP 0x3FF

char rx_buf[100];
char tx_buf[10];
volatile byte pos;
volatile boolean process_it;

byte values[2] = {0x00, 0x00};

int shunt_val = 0;
int therm_ctrl = 0;
int therm_pwr = 0;
int current_dem = 0;

unsigned int timestep = 0;

void setup()
{
//  pinMode(CURRENT_DEMAND_PIN, OUTPUT);
  analogWriteResolution(10);
  analogWrite(CURRENT_DEMAND_PIN, 0x0000);
  pinMode(CS_PIN, OUTPUT);
  digitalWrite(CS_PIN, LOW);
  
  Wire.begin(0x7F);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
//  Serial.begin(9600);           // start serial for output
}

void loop()
{
//  shunt_val = (int) analogRead(SHUNT_POS_PIN);
  delay(1);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{

  if (howMany == 0) { return; }

  digitalWrite(CS_PIN, HIGH);

  // RX message
  pos = 0;
  while(Wire.available()) // loop through all but the last
  {
    rx_buf[pos] = Wire.read(); // receive byte as a character
    pos++;
  }

  for (int i = 0; i <= 2; i++)
  {
//    Serial.println(rx_buf[i]);
  }

  // Handle message
  switch (rx_buf[0])
  {

//    case SHUNT_READ_MESG:
//      tx_buf[0] = (byte) (shunt_val >> 8);
//      tx_buf[1] = (byte) shunt_val;
//      Wire.write(tx_buf, 2);
//      break;
//    case THERMAL_CTRL_MESG:
//      therm_ctrl = (int) ((rx_buf[1] << 8) | rx_buf[2]);
//      analogWrite(THERMAL_CTRL_PIN, therm_ctrl);
//      break;
//    case THERMAL_PWR_MESG:
//      therm_pwr = (int) ((rx_buf[1] << 8) | rx_buf[2]);
//      analogWrite(THERMAL_PWR_PIN, therm_pwr);
//      break;
    case CURRENT_DEMAND_MESG:
      if (rx_buf[1] == (byte) 1) {
        analogWrite(CURRENT_DEMAND_PIN, (int)ONE_AMP);
      } else if (rx_buf[1] == (byte) 3) {
        analogWrite(CURRENT_DEMAND_PIN, (int)THREE_AMP);
      } else {
        analogWrite(CURRENT_DEMAND_PIN, 0x00);
      }
      break;
    default:
      // Faulty message , throw out.
      break;
    
  }

}
