# import serial
# from serial import Serial
# ser = serial.Serial('/dev/ttyUSB0')  # open serial port
# print(ser.name)         # check which port was really used
# ser.write(b'hello')     # write a string
# ser.readline(1000)
# ser.close()             # close port
import serial
import time

arduino = serial.Serial(port='/dev/cu.usbmodem14101', baudrate=115200, timeout=.1)


def read_data():

    time = 0
    voltage = 0
    current = 0

    data = arduino.read(6)
    data_arr = []
    for byte in data:
        data_arr.append(byte)

    if len(data_arr != 0)
        time  = (data_arr[0]<<8) + data_arr[1]
        voltage = (data_arr[2]<<8) + data_arr[3]
        current = (data_arr[4]<<8) + data_arr[5]

    return len(data_arr), time, float(voltage / 1000.0), float(current / 1000.0)

# while True:
#     # num = input("Enter a number: ")
#     num_bytes, time, voltage, current = write_read()
#     # a = write_read()
#     if num_bytes != 0:
#         print("Time: {}, Volts: {}, Amps: {}".format(time, voltage, current))
        
