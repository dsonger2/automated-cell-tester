import threading
import time
import tkinter as tk
from tkinter import ttk
from tkinter import *

import matplotlib
import serial

matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
import matplotlib.animation as animation
from matplotlib import style

DATA_MESG = 0
ERROR_MESG = 1
START_MESG = 1
STOP_MESG = 2

NO_TEST = 0
CAPACITY = 1
SOCOCV = 2
HPPC = 3

start_test = 0
stop_test = 0
test_type = 0

arduino = serial.Serial(port='/dev/cu.usbmodem14101', baudrate=115200, timeout=.1)

LARGE_FONT = ("Arial", 12)
style.use("ggplot")

# purple : #443b4f

f = Figure(figsize=(5, 5), dpi=100)
v_graph = f.add_subplot(2, 1, 1)
i_graph = f.add_subplot(2, 1, 2)

t_data = []
T_data = []
v_data = []
i_data = []

error = False

test_type = NO_TEST
rx_buffer = bytes([0x00]*10)

def read_data():

    time = 0
    type = 0
    temp = 0
    voltage = 0
    current = 0

    data = arduino.read(11)
    data_arr = []
    for byte in data:
        data_arr.append(byte)

    if len(data_arr) == 11:
        type = data_arr[0]
        time = (data_arr[1] << 24) + (data_arr[2] << 16) + (data_arr[3] << 8) + data_arr[4]
        voltage = (data_arr[5] << 8) + data_arr[6]
        current = (data_arr[7] << 8) + data_arr[8]
        if (current & (0x01 << 15)):
            current = (current ^ 0xFFFF) + 1
            current = -1 * current
        temp = (data_arr[9] << 8) + data_arr[10]

    return len(data_arr), type, float(time / 100.0), float(voltage / 1000.0), float(current / 1000.0), float(temp / 1000.0)


def usb_handler(name, error):
    test_cntr = 0
    type = 0

    while True:

        # Incoming messages
        num_bytes = 0
        if arduino.in_waiting:
            num_bytes, type, timestep, voltage, current, temp = read_data()

        # timer.sleep(50)

        if num_bytes != 0:

            # handle message
            if type == ERROR_MESG:
                # handle error message
                error = True
            elif type == STOP_MESG:
                filename = "celltest_" + str(test_cntr) + ".csv"
                with open(filename, 'w') as outfile:
                    outfile.write("Time,Voltage,Current,Temperature,\n")
                    for i in range(len(t_data)):
                        line = str(t_data[i]) + "," + str(v_data[i]) + "," + str(i_data[i]) + "," + str(T_data[i]) + ",\n"
                        outfile.write(line)

                test_cntr += 1
                t_data.clear()
                T_data.clear()
                v_data.clear()
                i_data.clear()

            t_data.append(timestep)
            T_data.append(temp)
            v_data.append(voltage)
            i_data.append(current)


def animate(i):

    t_arr = []
    v_arr = []
    i_arr = []

    if len(t_data) > 100:
        t_arr = t_data[len(t_data) - 100: -1:1]
        v_arr = v_data[len(t_data) - 100: -1:1]
        i_arr = i_data[len(t_data) - 100: -1:1]
    else:
        t_arr = t_data
        v_arr = v_data
        i_arr = i_data


    v_graph.clear()

    v_graph.plot(t_arr, v_arr, '#1B013D')
    v_graph.set_ylabel("voltage")

    i_graph.clear()

    i_graph.plot(t_arr, i_arr, '#1B013D')
    i_graph.set_ylabel("current")
    i_graph.set_xlabel("time")


def send_start_mesg(test_type, desired_temp):
    mesg = START_MESG
    # type = 0
    # temp = 0
    # if test_type == "Capacity":
    #     type = 0
    # elif test_type == "SoC-OCV":
    #     type = 1
    # elif test_type == "HPPC":
    #     type = 2
    #
    # if desired_temp == "Hot":
    #     temp = 0
    # elif desired_temp == "Room Temperature":
    #     temp = 1
    # elif desired_temp == "Cold":
    #     temp = 2
    #
    # my_byte = (temp << 2) | type
    rx_buffer = bytearray([mesg])

    arduino.write(rx_buffer)
    print("TXd ", rx_buffer)

    t_data.clear()
    T_data.clear()
    v_data.clear()
    i_data.clear()


def send_stop_mesg():
    rx_buffer = bytearray([STOP_MESG])
    arduino.write(rx_buffer)

    t_data.clear()
    T_data.clear()
    v_data.clear()
    i_data.clear()


class CellTester(tk.Tk):

    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.iconbitmap(self, "battery.ico")
        tk.Tk.wm_title(self, "Automated Thermal Cell Tester")
        self.geometry('800x480')

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (HomePage, TestStartPage, AnalyzeDataPage, GraphDataPage, ErrorPage):
            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(HomePage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    def text_update(self):
        if error:
            self.show_frame(ErrorPage)
        if len(t_data) > 0:
            self.frames[HomePage].temperatureText.configure(text=str(t_data[-1]) + " C")
            self.frames[HomePage].voltageText.configure(text=str(v_data[-1]) + "V")
            self.frames[HomePage].currentText.configure(text=str(i_data[-1]) + "A")


class HomePage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#404040")
        label = tk.Label(self, text="Home", font=LARGE_FONT)
        label.configure(background="#181850", foreground="#FFFFFF")
        label.pack(pady=10, padx=10)

        button = ttk.Button(self, text="Test Start Page",
                            command=lambda: controller.show_frame(TestStartPage))
        button.pack()

        button2 = ttk.Button(self, text="Analyze Cell Data",
                             command=lambda: controller.show_frame(AnalyzeDataPage))
        button2.pack()

        button3 = ttk.Button(self, text="Live Graph Data",
                             command=lambda: controller.show_frame(GraphDataPage))
        button3.pack()

        current = tk.Label(self, text="Current:", font=LARGE_FONT)
        current.configure(background="#002A6E",foreground="#FFFFFF")
        current.place(x=100, y=240)
        self.currentText = tk.Label(self, text="0")
        self.currentText.configure(background="#002A6E",foreground="#FFFFFF")
        self.currentText.place(x=115, y=260)

        voltage = tk.Label(self, text="Voltage:", font=LARGE_FONT)
        voltage.configure(background="#002A6E",foreground="#FFFFFF")
        voltage.place(x=350, y=240)
        self.voltageText = tk.Label(self, text="0")
        self.voltageText.configure(background="#002A6E",foreground="#FFFFFF")
        self.voltageText.place(x=365, y=260)

        temperature = tk.Label(self, text="Temperature:", font=LARGE_FONT)
        temperature.configure(background="#002A6E",foreground="#FFFFFF")
        temperature.place(x=600, y=240)
        self.temperatureText = tk.Label(self, text="0")
        self.temperatureText.configure(background="#002A6E",foreground="#FFFFFF")
        self.temperatureText.place(x=615, y=260)




class TestStartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#404040")
        label = tk.Label(self, text="Start a test", font=LARGE_FONT)
        label.pack(pady=10, padx=10)
        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(HomePage))
        button1.pack()
        #creating list for test options
        TEST_OPTIONS = ["Test Options","Capacity", "SoC-OCV", "HPPC"]
        test_options = ttk.Combobox(self, value=TEST_OPTIONS)
        test_options.current(0)
        test_options.pack(pady = 40, padx = 40)
        test_options.place(x=115, y=240)
        #creating list for temp options
        TEMP_OPTIONS = ["Temperature Options","Hot", "Room tempterature", "Cold"]
        temp_options = ttk.Combobox(self, value=TEMP_OPTIONS)
        temp_options.current(0)
        temp_options.pack(pady=40, padx=40)
        temp_options.place(x=485, y=240)
        #test start button
        button2 = ttk.Button(self, text="Start Test",
                             command=lambda: send_start_mesg(test_type=test_options.get(), desired_temp=temp_options.get()))
        button2.pack(pady = 30, padx = 30)
        button2.place(x=350, y=400)
        button3 = ttk.Button(self,text = "Stop Test",command = lambda: send_stop_mesg())
        button3.pack(pady = 40, padx = 40)
        button3.place(x=350, y=425)


class AnalyzeDataPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#404040")
        label = tk.Label(self, text="Analyze Cell Data", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(HomePage))
        button1.pack()

        # button2 = ttk.Button(self, text="Page One",
        #                      command=lambda: controller.show_frame(TestStartPage))
        # button2.pack()


class GraphDataPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#443B4F")
        label = tk.Label(self, text="Live Graph Data", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(HomePage))
        button1.pack()

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        toolbar = NavigationToolbar2Tk(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


class ErrorPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#3F0000")
        label = tk.Label(self, text="ERROR", font=LARGE_FONT)
        label.configure(background="#000000", foreground="#7F1010")
        label.pack(pady=10, padx=10)

        button = ttk.Button(self, text="Home Page",
                            command=lambda: controller.show_frame(HomePage))
        button.pack()

        current = tk.Label(self, text="Current:", font=LARGE_FONT)
        current.configure(background="#002A6E",foreground="#FFFFFF")
        current.place(x=100, y=240)
        self.currentText = tk.Label(self, text="0")
        self.currentText.configure(background="#002A6E",foreground="#FFFFFF")
        self.currentText.place(x=115, y=260)

        voltage = tk.Label(self, text="Voltage:", font=LARGE_FONT)
        voltage.configure(background="#002A6E",foreground="#FFFFFF")
        voltage.place(x=350, y=240)
        self.voltageText = tk.Label(self, text="0")
        self.voltageText.configure(background="#002A6E",foreground="#FFFFFF")
        self.voltageText.place(x=365, y=260)

        temperature = tk.Label(self, text="Temperature:", font=LARGE_FONT)
        temperature.configure(background="#002A6E",foreground="#FFFFFF")
        temperature.place(x=600, y=240)
        self.temperatureText = tk.Label(self, text="0")
        self.temperatureText.configure(background="#002A6E",foreground="#FFFFFF")
        self.temperatureText.place(x=615, y=260)

def main():

    app = CellTester()

    ani = animation.FuncAnimation(f, animate, interval=100)
    num_mesgs = 0
    while num_mesgs <= 3:
        num_bytes, type, timestep, voltage, current, temp = read_data()
        if num_bytes != 0:
            num_mesgs += 1

    usb_thread = threading.Thread(target=usb_handler, args=(1, error))
    usb_thread.start()

    while True:
        app.update_idletasks()
        app.update()
        app.text_update()

    usb_thread.join()


if __name__=="__main__":
    main()