#include <TimeLib.h>

#define DISCHARGER_DONE 0
#define NOT_DONE 1

unsigned long start_time = 0;
unsigned long prev_time;
unsigned long time_step = 0;
unsigned int cntr = 0;
int state = 0;

void printDigits(int digits) {
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void digitalClockDisplay() {
/* 
  time_step = millis() - prev_millis;
  prev_millis = millis();
  Serial.println(time_step);
  */
  /*
  // digital clock display of the time
  Serial.print(hour(time));
  printDigits(minute(time));
  printDigits(second(time));
  Serial.print(" ");
  Serial.print(dayStr(weekday(time)));
  Serial.print(" ");
  Serial.print(day(time));
  Serial.print(" ");
  Serial.print(monthStr(month(time)));
  Serial.print(" ");
  Serial.print(year(time)); 
  Serial.println(); 
  */
  
}

int check_state()
{

    int current = millis();
    if ((current - start_time) >= 19998){
      Serial.println(current - start_time);
      start_time = current;
      return DISCHARGER_DONE;
    }

    return NOT_DONE;
  
}

void setup()
{
  Serial.begin(9600);
  delay(3000);
}

void loop()
{

  if (cntr == 0) {
    start_time = millis();
    cntr++;
  }

  state = check_state();

  if (state == DISCHARGER_DONE){
    Serial.println("Done");
  }
  
  delay(1000);
  
}
