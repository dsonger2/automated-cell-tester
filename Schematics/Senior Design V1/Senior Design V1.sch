EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eec:MJ21194G Q1
U 1 1 61514E28
P 5200 4150
F 0 "Q1" H 5444 4196 50  0000 L CNN
F 1 "MJ21194G" H 5444 4105 50  0000 L CNN
F 2 "On_Semiconductor-MJ21194G-*" H 5200 4650 50  0001 L CNN
F 3 "http://www.onsemi.cn/pub_link/Collateral/MJ21193-D.PDF" H 5200 4750 50  0001 L CNN
F 4 "Manufacturer URL" H 5200 4850 50  0001 L CNN "Component Link 1 Description"
F 5 "http://www.onsemi.com/" H 5200 4950 50  0001 L CNN "Component Link 1 URL"
F 6 "Package Specification" H 5200 5050 50  0001 L CNN "Component Link 3 Description"
F 7 "http://www.onsemi.com/pub_link/Collateral/1-07.PDF" H 5200 5150 50  0001 L CNN "Component Link 3 URL"
F 8 "Rev. 6" H 5200 5250 50  0001 L CNN "Datasheet Version"
F 9 "16" H 5200 5350 50  0001 L CNN "IC Continuous A"
F 10 "Through Hole" H 5200 5450 50  0001 L CNN "Mounting Technology"
F 11 "2-Pin Transistor Outline, Body 39.37 x 26.67 mm, Pitch 10.92 mm" H 5200 5550 50  0001 L CNN "Package Description"
F 12 "Rev. Z, 05/1988" H 5200 5650 50  0001 L CNN "Package Version"
F 13 "Tray" H 5200 5750 50  0001 L CNN "Packing"
F 14 "NPN" H 5200 5850 50  0001 L CNN "Polarity"
F 15 "Trans" H 5200 5950 50  0001 L CNN "category"
F 16 "9d749437038dec4a" H 5200 6050 50  0001 L CNN "library id"
F 17 "On Semiconductor" H 5200 6150 50  0001 L CNN "manufacturer"
F 18 "TO-204-3-1-07" H 5200 6250 50  0001 L CNN "package"
F 19 "Yes" H 5200 6350 50  0001 L CNN "rohs"
F 20 "25197A6D-B529-46A2-ABCE-3FF548204E5F" H 5200 6450 50  0001 L CNN "vault revision"
F 21 "4" H 5200 6550 50  0001 L CNN "fT Min MHz"
F 22 "75" H 5200 6650 50  0001 L CNN "hFE Max"
F 23 "yes" H 5200 6750 50  0001 L CNN "imported"
	1    5200 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DGS Q2
U 1 1 615185B4
P 5100 3750
F 0 "Q2" H 5304 3796 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 5304 3705 50  0000 L CNN
F 2 "" H 5300 3850 50  0001 C CNN
F 3 "~" H 5100 3750 50  0001 C CNN
	1    5100 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_DGS Q3
U 1 1 6151CBCE
P 5100 4550
F 0 "Q3" H 5305 4596 50  0000 L CNN
F 1 "Q_PMOS_DGS" H 5305 4505 50  0000 L CNN
F 2 "" H 5300 4650 50  0001 C CNN
F 3 "~" H 5100 4550 50  0001 C CNN
	1    5100 4550
	1    0    0    -1  
$EndComp
$Comp
L Analog_DAC:TLV5627CD U1
U 1 1 6151F3CF
P 1750 3850
F 0 "U1" H 1750 4631 50  0000 C CNN
F 1 "TLV5627CD" H 1750 4540 50  0000 C CNN
F 2 "" H 1750 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlv5627.pdf" H 1750 3850 50  0001 C CNN
	1    1750 3850
	1    0    0    -1  
$EndComp
$Comp
L AD8531ARZ:AD8531ARZ U2
U 1 1 61523791
P 3700 3650
F 0 "U2" H 3700 4320 50  0000 C CNN
F 1 "AD8531ARZ" H 3700 4229 50  0000 C CNN
F 2 "SOIC127P600X175-8N" H 3700 3650 50  0001 L BNN
F 3 "" H 3700 3650 50  0001 L BNN
F 4 "Analog Devices" H 3700 3650 50  0001 L BNN "SUPPLIER"
F 5 "AD8531ARZ" H 3700 3650 50  0001 L BNN "MPN"
F 6 "9605479" H 3700 3650 50  0001 L BNN "OC_FARNELL"
F 7 "19M0961" H 3700 3650 50  0001 L BNN "OC_NEWARK"
F 8 "SOIC-8" H 3700 3650 50  0001 L BNN "PACKAGE"
	1    3700 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3550 3000 3550
Wire Wire Line
	5200 3950 5200 4150
Wire Wire Line
	5200 4350 5200 4150
Connection ~ 5200 4150
Wire Wire Line
	4400 3350 4400 2800
Wire Wire Line
	4400 2800 2850 2800
Wire Wire Line
	2850 2800 2850 3650
Wire Wire Line
	2850 3650 3000 3650
$Comp
L Device:R R?
U 1 1 61530B78
P 4750 3350
F 0 "R?" V 4543 3350 50  0000 C CNN
F 1 "1k" V 4634 3350 50  0000 C CNN
F 2 "" V 4680 3350 50  0001 C CNN
F 3 "~" H 4750 3350 50  0001 C CNN
	1    4750 3350
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61531B7B
P 4750 4750
F 0 "R?" V 4543 4750 50  0000 C CNN
F 1 "20" V 4634 4750 50  0000 C CNN
F 2 "" V 4680 4750 50  0001 C CNN
F 3 "~" H 4750 4750 50  0001 C CNN
	1    4750 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 3350 4600 3350
Connection ~ 4400 3350
Wire Wire Line
	4900 3350 5200 3350
Wire Wire Line
	5200 3350 5200 3550
Wire Wire Line
	5200 4750 4900 4750
Wire Wire Line
	4600 4750 4600 3350
Connection ~ 4600 3350
Wire Wire Line
	4900 3750 4900 4550
$Comp
L Device:R R1
U 1 1 61533CA5
P 5400 1950
F 0 "R1" H 5470 1996 50  0000 L CNN
F 1 "0.25" H 5470 1905 50  0000 L CNN
F 2 "" V 5330 1950 50  0001 C CNN
F 3 "~" H 5400 1950 50  0001 C CNN
	1    5400 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 6153825A
P 5400 850
F 0 "J1" V 5338 898 50  0000 L CNN
F 1 "Cell Connection" V 5247 898 50  0000 L CNN
F 2 "" H 5400 850 50  0001 C CNN
F 3 "~" H 5400 850 50  0001 C CNN
	1    5400 850 
	0    1    -1   0   
$EndComp
$Comp
L Device:Fuse F1
U 1 1 6153B929
P 5400 1350
F 0 "F1" H 5460 1396 50  0000 L CNN
F 1 "Fuse" H 5460 1305 50  0000 L CNN
F 2 "" V 5330 1350 50  0001 C CNN
F 3 "~" H 5400 1350 50  0001 C CNN
	1    5400 1350
	1    0    0    -1  
$EndComp
$Comp
L CMX100D10:CMX100D10 K1
U 1 1 6153F66A
P 5600 3300
F 0 "K1" H 6142 2735 50  0000 C CNN
F 1 "CMX100D10" H 6142 2826 50  0000 C CNN
F 2 "CMX100D10" H 6750 3400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/CMX100D10.pdf" H 6750 3300 50  0001 L CNN
F 4 "Sensata / Crydom 10 A rms SPNO Solid State Relay, PCB Mount, MOSFET, 100 V dc Maximum Load" H 6750 3200 50  0001 L CNN "Description"
F 5 "26.7" H 6750 3100 50  0001 L CNN "Height"
F 6 "CRYDOM" H 6750 3000 50  0001 L CNN "Manufacturer_Name"
F 7 "CMX100D10" H 6750 2900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "558-CMX100D10" H 6750 2800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Crydom/CMX100D10?qs=ftmCDQnBrlMvJvu9NkGZVg%3D%3D" H 6750 2700 50  0001 L CNN "Mouser Price/Stock"
F 10 "CMX100D10" H 6750 2600 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/cmx100d10/crydom" H 6750 2500 50  0001 L CNN "Arrow Price/Stock"
	1    5600 3300
	1    0    0    1   
$EndComp
Wire Wire Line
	5400 3000 5600 3000
Wire Wire Line
	5400 2100 5400 2150
Wire Wire Line
	5600 3100 5400 3100
Wire Wire Line
	5400 3100 5400 3950
$Comp
L Amplifier_Operational:MCP6V67EMS U?
U 1 1 6154B9C0
P 6400 1700
F 0 "U?" H 6400 2067 50  0000 C CNN
F 1 "MCP6V67EMS" H 6400 1976 50  0000 C CNN
F 2 "" H 6400 1700 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MCP6V66-Family-Data-Sheet-DS20006266A.pdf" H 6400 1700 50  0001 C CNN
	1    6400 1700
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:MCP6V67EMS U?
U 1 1 615585ED
P 6400 2350
F 0 "U?" H 6400 2717 50  0000 C CNN
F 1 "MCP6V67EMS" H 6400 2626 50  0000 C CNN
F 2 "" H 6400 2350 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MCP6V66-Family-Data-Sheet-DS20006266A.pdf" H 6400 2350 50  0001 C CNN
	1    6400 2350
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:MCP6V67EMS U?
U 1 1 6155A645
P 7600 2050
F 0 "U?" H 7600 2417 50  0000 C CNN
F 1 "MCP6V67EMS" H 7600 2326 50  0000 C CNN
F 2 "" H 7600 2050 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MCP6V66-Family-Data-Sheet-DS20006266A.pdf" H 7600 2050 50  0001 C CNN
	1    7600 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 1800 5400 1600
Wire Wire Line
	5400 1200 5400 1050
Wire Wire Line
	6100 1600 5400 1600
Connection ~ 5400 1600
Wire Wire Line
	5400 1600 5400 1500
Wire Wire Line
	6100 1800 6100 1950
Wire Wire Line
	6100 1950 6700 1950
Wire Wire Line
	6700 1950 6700 1700
Wire Wire Line
	6100 2450 6100 2600
Wire Wire Line
	6100 2600 6700 2600
Wire Wire Line
	6700 2600 6700 2350
Wire Wire Line
	6100 2250 5400 2250
Connection ~ 5400 2250
Wire Wire Line
	5400 2250 5400 2600
$Comp
L Device:R R4
U 1 1 61567B38
P 7300 1350
F 0 "R4" H 7370 1396 50  0000 L CNN
F 1 "200" H 7370 1305 50  0000 L CNN
F 2 "" V 7230 1350 50  0001 C CNN
F 3 "~" H 7300 1350 50  0001 C CNN
	1    7300 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 61568FF6
P 7000 1950
F 0 "R2" V 6793 1950 50  0000 C CNN
F 1 "50" V 6884 1950 50  0000 C CNN
F 2 "" V 6930 1950 50  0001 C CNN
F 3 "~" H 7000 1950 50  0001 C CNN
	1    7000 1950
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 615698F4
P 7000 2350
F 0 "R3" V 6793 2350 50  0000 C CNN
F 1 "50" V 6884 2350 50  0000 C CNN
F 2 "" V 6930 2350 50  0001 C CNN
F 3 "~" H 7000 2350 50  0001 C CNN
	1    7000 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 61569EA6
P 7600 2350
F 0 "R5" V 7393 2350 50  0000 C CNN
F 1 "200" V 7484 2350 50  0000 C CNN
F 2 "" V 7530 2350 50  0001 C CNN
F 3 "~" H 7600 2350 50  0001 C CNN
	1    7600 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	6700 1950 6850 1950
Connection ~ 6700 1950
Wire Wire Line
	7150 1950 7300 1950
Wire Wire Line
	7300 1500 7300 1950
Connection ~ 7300 1950
Wire Wire Line
	6700 2350 6850 2350
Connection ~ 6700 2350
Wire Wire Line
	7150 2350 7300 2350
Wire Wire Line
	7750 2350 7900 2350
Wire Wire Line
	7900 2350 7900 2200
Wire Wire Line
	7300 2150 7300 2350
Connection ~ 7300 2350
Wire Wire Line
	7300 2350 7450 2350
$Comp
L power:GND #PWR?
U 1 1 6156FEB5
P 7100 1200
F 0 "#PWR?" H 7100 950 50  0001 C CNN
F 1 "GND" H 7105 1027 50  0000 C CNN
F 2 "" H 7100 1200 50  0001 C CNN
F 3 "" H 7100 1200 50  0001 C CNN
	1    7100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1200 7100 1200
$Comp
L power:GND #PWR?
U 1 1 61572A00
P 5400 4800
F 0 "#PWR?" H 5400 4550 50  0001 C CNN
F 1 "GND" H 5405 4627 50  0000 C CNN
F 2 "" H 5400 4800 50  0001 C CNN
F 3 "" H 5400 4800 50  0001 C CNN
	1    5400 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4350 5400 4800
$Comp
L power:GND #PWR?
U 1 1 61574780
P 2950 4400
F 0 "#PWR?" H 2950 4150 50  0001 C CNN
F 1 "GND" H 2955 4227 50  0000 C CNN
F 2 "" H 2950 4400 50  0001 C CNN
F 3 "" H 2950 4400 50  0001 C CNN
	1    2950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4250 2950 4250
Wire Wire Line
	2950 4250 2950 4400
$Comp
L Analog_ADC:ADC121C021CIMM U?
U 1 1 615763E0
P 8800 2300
F 0 "U?" H 8800 2781 50  0000 C CNN
F 1 "ADC121C021CIMM" H 8800 2690 50  0000 C CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 9600 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/adc121c021.pdf" H 8800 2400 50  0001 C CNN
	1    8800 2300
	1    0    0    -1  
$EndComp
$Comp
L Analog_ADC:ADC121C021CIMM U?
U 1 1 61578D93
P 8800 1250
F 0 "U?" H 8800 1731 50  0000 C CNN
F 1 "ADC121C021CIMM" H 8800 1640 50  0000 C CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 9600 900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/adc121c021.pdf" H 8800 1350 50  0001 C CNN
	1    8800 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1550 8200 1550
Wire Wire Line
	8200 1550 8200 1150
Wire Wire Line
	8200 1150 8400 1150
Wire Wire Line
	8400 2200 7900 2200
Connection ~ 7900 2200
Wire Wire Line
	7900 2200 7900 2050
$Comp
L power:GND #PWR?
U 1 1 6157EA80
P 8800 1550
F 0 "#PWR?" H 8800 1300 50  0001 C CNN
F 1 "GND" H 8805 1377 50  0000 C CNN
F 2 "" H 8800 1550 50  0001 C CNN
F 3 "" H 8800 1550 50  0001 C CNN
	1    8800 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6157F136
P 8800 2600
F 0 "#PWR?" H 8800 2350 50  0001 C CNN
F 1 "GND" H 8805 2427 50  0000 C CNN
F 2 "" H 8800 2600 50  0001 C CNN
F 3 "" H 8800 2600 50  0001 C CNN
	1    8800 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1550 6700 1700
Connection ~ 6700 1700
Wire Wire Line
	9200 1150 9700 1150
Wire Wire Line
	9700 1150 9700 2200
Wire Wire Line
	9700 2200 9200 2200
Wire Wire Line
	9200 1250 9600 1250
Wire Wire Line
	9600 1250 9600 2300
Wire Wire Line
	9200 2300 9600 2300
$Comp
L power:GND #PWR?
U 1 1 6159115F
P 5300 1050
F 0 "#PWR?" H 5300 800 50  0001 C CNN
F 1 "GND" H 5305 877 50  0000 C CNN
F 2 "" H 5300 1050 50  0001 C CNN
F 3 "" H 5300 1050 50  0001 C CNN
	1    5300 1050
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATmega:ATmega32U4-MU U?
U 1 1 6159359D
P 8750 5150
F 0 "U?" V 8796 3306 50  0000 R CNN
F 1 "ATmega32U4-MU" V 8705 3306 50  0000 R CNN
F 2 "Package_DFN_QFN:QFN-44-1EP_7x7mm_P0.5mm_EP5.2x5.2mm" H 8750 5150 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf" H 8750 5150 50  0001 C CNN
	1    8750 5150
	0    1    -1   0   
$EndComp
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U?
U 1 1 615983B8
P 2700 5800
F 0 "U?" H 2171 5846 50  0000 R CNN
F 1 "ATtiny85-20PU" H 2171 5755 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2700 5800 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 2700 5800 50  0001 C CNN
	1    2700 5800
	1    0    0    -1  
$EndComp
Text GLabel 3900 5500 0    50   Input ~ 0
SDA
Text GLabel 3900 5700 0    50   Input ~ 0
SCL
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 615A281B
P 4850 5800
F 0 "J2" H 4878 5776 50  0000 L CNN
F 1 "Thermistor Voltage Divider" H 4878 5685 50  0000 L CNN
F 2 "" H 4850 5800 50  0001 C CNN
F 3 "~" H 4850 5800 50  0001 C CNN
	1    4850 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5800 4650 5800
$Comp
L power:GND #PWR?
U 1 1 615A7C71
P 4650 5900
F 0 "#PWR?" H 4650 5650 50  0001 C CNN
F 1 "GND" H 4655 5727 50  0000 C CNN
F 2 "" H 4650 5900 50  0001 C CNN
F 3 "" H 4650 5900 50  0001 C CNN
	1    4650 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 615A83D3
P 2700 6400
F 0 "#PWR?" H 2700 6150 50  0001 C CNN
F 1 "GND" H 2705 6227 50  0000 C CNN
F 2 "" H 2700 6400 50  0001 C CNN
F 3 "" H 2700 6400 50  0001 C CNN
	1    2700 6400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 615B25D0
P 4850 5500
F 0 "J?" H 4878 5476 50  0000 L CNN
F 1 "Peltier Device Control Signals" H 4878 5385 50  0000 L CNN
F 2 "" H 4850 5500 50  0001 C CNN
F 3 "~" H 4850 5500 50  0001 C CNN
	1    4850 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5600 4250 5600
Wire Wire Line
	4400 5600 4400 5500
Wire Wire Line
	4400 5500 4650 5500
Wire Wire Line
	3300 5900 4500 5900
Wire Wire Line
	4500 5900 4500 5600
Wire Wire Line
	4500 5600 4650 5600
$Comp
L SamacSys_Parts:BQ24179YBGR IC?
U 1 1 615BCFD9
P 4050 600
F 0 "IC?" V 4704 728 50  0000 L CNN
F 1 "BQ24179YBGR" V 4795 728 50  0000 L CNN
F 2 "BGA56C40P7X8_286X328X50" H 5300 700 50  0001 L CNN
F 3 "https://www.ti.com/lit/ds/symlink/bq24179.pdf?ts=1620434088502&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FBQ24179" H 5300 600 50  0001 L CNN
F 4 "Battery Management Integrated, NVDC, 5-A 1-cell to 4-cell switch-mode buck-boost battery charger 56-DSBGA -40 to 85" H 5300 500 50  0001 L CNN "Description"
F 5 "0.5" H 5300 400 50  0001 L CNN "Height"
F 6 "595-BQ24179YBGR" H 5300 300 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/BQ24179YBGR?qs=iLbezkQI%252Bsjk7FiJgAMAJg%3D%3D" H 5300 200 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 5300 100 50  0001 L CNN "Manufacturer_Name"
F 9 "BQ24179YBGR" H 5300 0   50  0001 L CNN "Manufacturer_Part_Number"
	1    4050 600 
	0    1    1    0   
$EndComp
$Comp
L CMX100D10:CMX100D10 K?
U 1 1 615C7427
P 5150 2300
F 0 "K?" H 5692 2565 50  0000 C CNN
F 1 "CMX100D10" H 5692 2474 50  0000 C CNN
F 2 "CMX100D10" H 6300 2400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/CMX100D10.pdf" H 6300 2300 50  0001 L CNN
F 4 "Sensata / Crydom 10 A rms SPNO Solid State Relay, PCB Mount, MOSFET, 100 V dc Maximum Load" H 6300 2200 50  0001 L CNN "Description"
F 5 "26.7" H 6300 2100 50  0001 L CNN "Height"
F 6 "CRYDOM" H 6300 2000 50  0001 L CNN "Manufacturer_Name"
F 7 "CMX100D10" H 6300 1900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "558-CMX100D10" H 6300 1800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Crydom/CMX100D10?qs=ftmCDQnBrlMvJvu9NkGZVg%3D%3D" H 6300 1700 50  0001 L CNN "Mouser Price/Stock"
F 10 "CMX100D10" H 6300 1600 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/cmx100d10/crydom" H 6300 1500 50  0001 L CNN "Arrow Price/Stock"
	1    5150 2300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5400 2600 5150 2600
Connection ~ 5400 2600
Wire Wire Line
	5400 2600 5400 3000
Wire Wire Line
	3450 500  3450 600 
Wire Wire Line
	2050 2000 2050 2150
Wire Wire Line
	2050 2150 5400 2150
Connection ~ 5400 2150
Wire Wire Line
	5400 2150 5400 2250
Wire Wire Line
	5150 2500 5250 2500
Wire Wire Line
	5250 2500 5250 500 
Wire Wire Line
	3450 500  5250 500 
$Comp
L Connector:AVR-ISP-6 J?
U 1 1 615E4696
P 3750 6950
F 0 "J?" H 3421 7046 50  0000 R CNN
F 1 "AVR-ISP-6" H 3421 6955 50  0000 R CNN
F 2 "" V 3500 7000 50  0001 C CNN
F 3 " ~" H 2475 6400 50  0001 C CNN
	1    3750 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 6750 4250 6750
Wire Wire Line
	4250 6750 4250 5600
Connection ~ 4250 5600
Wire Wire Line
	4250 5600 4400 5600
Wire Wire Line
	4150 6850 4350 6850
Wire Wire Line
	4350 6850 4350 5500
Wire Wire Line
	3300 5500 4350 5500
Wire Wire Line
	4150 6950 4400 6950
Wire Wire Line
	4400 6950 4400 5700
Wire Wire Line
	3300 5700 4400 5700
Wire Wire Line
	4150 7050 4500 7050
Wire Wire Line
	4500 7050 4500 6000
Wire Wire Line
	4500 6000 3300 6000
$Comp
L power:GND #PWR?
U 1 1 615F1208
P 3650 7350
F 0 "#PWR?" H 3650 7100 50  0001 C CNN
F 1 "GND" H 3655 7177 50  0000 C CNN
F 2 "" H 3650 7350 50  0001 C CNN
F 3 "" H 3650 7350 50  0001 C CNN
	1    3650 7350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
